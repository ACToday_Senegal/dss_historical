# README #


### What is this repository for? ###

* Climate-Agriculture Modeling Decision Support Tool (DSS) is a tool designed to guide decision-makers in adopting appropriate crop and management practices that can improve crop yields given a seasonal climatic condition.
* Smart planning of annual crop production requires consideration of possible scenarios. The DSS tool adopts crop simulation models included in the DSSAT package (Decision Support System for Agrotechnology Transfer). 
* The methodology was developed by the IRI (International Research Institute for Climate and Society / Columbia University)in collaboration with the Institut Sénégalais de Recherches Agricoles (ISRA), Senegal.
* The purpose of this tool is to support decision-making of the producer or technical advisor, which facilitates discussion of optimal production strategies, risks of technology adoption, and evaluation of long-term effects, considering interactions of various factors.



### How do I get set up? ###

* Instruction on how to install DSS tool can be found in https://docs.google.com/document/d/1DHjAAhXxH776ZabJ9UWCft1UGBmXM-sU/edit


### Who do I talk to? ###

* Repo owner: Eunjin Han (https://iri.columbia.edu/contact/staff-directory/eunjin-han/), IRI, Columbia University, eunjin@iri.columbia.edu)
* Other contributors:
* Walter Baethgen, International Research Institute for Climate and Society (IRI), Columbia University, USA
* James Hansen, International Research Institute for Climate and Society (IRI), Columbia University, USA
* Adama Faye, Institut Sénégalais de Recherches Agricoles (ISRA), Senegal
* Mbaye Diop, Institut Sénégalais de Recherches Agricoles (ISRA), Senegal