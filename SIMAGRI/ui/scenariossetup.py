# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._compact import *
# from SIMAGRI.process.diseasemodule import DISEASEPROC
from SIMAGRI.process.displaymodule import DISPLAY_PROC
# from SIMAGRI.process.downscalingmodule import DOWNSCALING_PROC
from SIMAGRI.process.simagrimodule import SIMAGRIPROC
from SIMAGRI.process.errormessages import ModuleErrorMessage

from threading import Thread
from time import sleep

class ScenariosSetupUI:
  __UI_Name__ = loc.ScenariosSetup.Title

  _UIParent = None
  _UINootbook = None

  _Setting = None

  _SIMAGRIProc = None
  # _DOWNSCALING_Proc = None
  _DISPLAY_Proc = None
  _ModErrMsg = None

  def __init__(self, setting, parent, notebook):
    print ("init %s Tab" % (self.__UI_Name__))

    self._UIParent = parent

    # set uivar variable for saving variables in simulation setup ui
    self._Setting = setting

    # set SIMAGRI Process Module
    self._SIMAGRIProc = SIMAGRIPROC(setting, parent)

    # # set Temporal Downscaling Process Module
    # self._DOWNSCALING_Proc = DOWNSCALING_PROC(setting, parent)

    # set DISPLAY Process Module
    self._DISPLAY_Proc = DISPLAY_PROC(setting, parent)

    self._ModErrMsg = ModuleErrorMessage(parent)

    # =========================5th  page for "DSSAT setup"
    page5 = notebook.add(self.__UI_Name__)
    notebook.tab(self.__UI_Name__).focus_set()

    # 1) ADD SCROLLED FRAME
    sf_p5 = Pmw.ScrolledFrame(page5)  # ,usehullsize=1, hull_width=700, hull_height=220)
    sf_p5.pack(padx = 5, pady = 3, fill = 'both', expand = YES)

    group51 = Pmw.Group(sf_p5.interior(), tag_text = 'Working directory')
    group51.pack(fill = 'x', expand = 1, anchor = N, padx = 10, pady = 5)
    # Working directory
    fm51 = Frame(group51.interior())
    self.WDir_label0 = Label(fm51, text = 'Working directory:', padx = 5, pady = 5)
    self.WDir_label0.pack(fill = 'x', side = LEFT, anchor = W, padx = 10, pady = 5)
    self.WDir_label = Label(fm51, text = loc.Msg.Not_added_abbr, relief='sunken', padx = 5, pady = 5)
    self.WDir_label.pack(fill = 'x', side = LEFT, anchor = W, padx = 10, pady = 5)
    fm51.pack(side = TOP)
    fm52 = Frame(group51.interior())
    # Create button to get file path
    self.file_button = tkinter.Button(fm52,
                                      text = loc.ScenariosSetup.MSG_Click_Select_Working_Directory,
                                      command = self.getWdir, bg = 'gray70').pack(side = LEFT, anchor = N)
    self.copy_button = tkinter.Button(fm52,
                                      text = loc.ScenariosSetup.MSG_Copy_SIMAGRI_module_files,
                                      command = self.copyModule, bg = 'gray70').pack(side = LEFT, anchor = N)
    fm52.pack(side = TOP)
    fm53 = Frame(group51.interior())
    WDir_label2 = Label(fm53, text = loc.ScenariosSetup.MSG_Note_Working_Directory1).pack(side = TOP, anchor = W)
    WDir_label3 = Label(fm53,
                        text = loc.ScenariosSetup.MSG_Note_Working_Directory2).pack(side = TOP, anchor = W)
    fm53.pack(side = TOP)

    # # 3rd group for threshold of water stress index"
    # group53 = Pmw.Group(sf_p5.interior(), tag_text = 'Threshold for water stress index')
    # group53.pack(fill = 'x', expand = 1, anchor = N, padx = 10, pady = 5)
    # self._Setting.ScenariosSetup.WSI_threshold = Pmw.EntryField(group53.interior(), # self.wsi_threshold
    #                                     labelpos = 'w', label_text = 'Threshold water stress (0~1) to compute prob. of exceeding it? ',
    #                                     value='0.5',  # by default =0.5
    #                                     validate = {'validator': 'real', 'min': 0.01, 'max': 1, 'minstrict': 0})
    # self._Setting.ScenariosSetup.WSI_threshold.pack(side = TOP, padx = 5, pady = 5)

    # 2nd group for What-If Scenarios"
    group52 = Pmw.Group(sf_p5.interior(), tag_text = 'What-If scenarios')
    group52.pack(fill = 'x', expand = 1, anchor = N, padx = 10, pady = 5)
    ##        self.nscenario = Pmw.EntryField(group52.interior(), labelpos = 'w',
    ##            label_text = 'How many scenarios? ',
    ##            validate = {'validator': 'numeric', 'min' : 1, 'max' : 5, 'minstrict' : 0})
    ##        self.nscenario.pack( side = TOP, padx = 5, pady = 5)
    fm53 = Frame(group52.interior())
    label51 = Label(fm53, text = 'Scenario Name').pack(side = TOP)
    label510 = Label(fm53, text = '(4char) ').pack(side = TOP)
    self._Setting.ScenariosSetup.WIfS_name1 = Pmw.EntryField(fm53, labelpos = 'w', entry_width = 10, # self.name1
                                                             label_text = '1:',
                                                             validate = {'validator': 'alphanumeric', 'min': 1, 'max': 10, 'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_name1.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_name2 = Pmw.EntryField(fm53,  # self.name2
                                                             labelpos = 'w', entry_width = 10,
                                                             label_text = '2:',
                                                             validate = {'validator': 'alphanumeric', 'min': 1, 'max': 10, 'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_name2.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_name3 = Pmw.EntryField(fm53, labelpos = 'w', entry_width = 10,
                                                             label_text = '3:',
                                                             validate = {'validator': 'alphanumeric', 'min': 1, 'max': 10, 'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_name3.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_name4 = Pmw.EntryField(fm53, labelpos = 'w', entry_width = 10,
                                                             label_text = '4:',
                                                             validate = {'validator': 'alphanumeric', 'min': 1, 'max': 10, 'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_name4.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_name5 = Pmw.EntryField(fm53, labelpos = 'w', entry_width = 10,
                                                             label_text = '5:',
                                                             validate = {'validator': 'alphanumeric', 'min': 1, 'max': 10, 'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_name5.pack(side = TOP, padx = 5, pady = 5)
    fm53.pack(side = LEFT)

    # -2nd frame
    fm54 = Frame(group52.interior())
    label52 = Label(fm54, text = '     ').pack(side = TOP)
    label520 = Label(fm54, text = '     ').pack(side = TOP)
    # Create button to get file path
    self.SNX_button1 = tkinter.Button(fm54, text = 'Run DSSAT',
                                        command=lambda: self.runDSSAT(self._Setting.ScenariosSetup.WIfS_name1.getvalue(), 1),
                                        bg='black', fg='white').pack(side = TOP, anchor = N, padx = 5, pady = 2)
    self.SNX_button2 = tkinter.Button(fm54, text = 'Run DSSAT',
                                        command=lambda: self.runDSSAT(self._Setting.ScenariosSetup.WIfS_name2.getvalue(), 2),
                                        bg='black', fg='white').pack(side = TOP, anchor = N, padx = 5, pady = 2)
    self.SNX_button3 = tkinter.Button(fm54, text = 'Run DSSAT',
                                        command=lambda: self.runDSSAT(self._Setting.ScenariosSetup.WIfS_name3.getvalue(), 3),
                                        bg='black', fg='white').pack(side = TOP, anchor = N, padx = 5, pady = 2)
    self.SNX_button4 = tkinter.Button(fm54, text = 'Run DSSAT',
                                        command=lambda: self.runDSSAT(self._Setting.ScenariosSetup.WIfS_name4.getvalue(), 4),
                                        bg='black', fg='white').pack(side = TOP, anchor = N, padx = 5, pady = 2)
    self.SNX_button5 = tkinter.Button(fm54, text = 'Run DSSAT',
                                        command=lambda: self.runDSSAT(self._Setting.ScenariosSetup.WIfS_name5.getvalue(), 5), 
                                        bg='black', fg='white').pack(side = TOP, anchor = N, padx = 5, pady = 2)
    fm54.pack(side = LEFT)
    # -3rd frame
    fm55 = Frame(group52.interior())
    label53 = Label(fm55, text = '  ').pack(side = TOP)
    label530 = Label(fm55, text = 'Crop').pack(side = TOP)
    self._Setting.ScenariosSetup.WIfS_crop1 = Label(fm55, text = loc.Msg.Not_added_abbr, relief='sunken')
    self._Setting.ScenariosSetup.WIfS_crop1.pack(side = TOP, padx = 1, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop2 = Label(fm55, text = loc.Msg.Not_added_abbr, relief='sunken')
    self._Setting.ScenariosSetup.WIfS_crop2.pack(side = TOP, padx = 1, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop3 = Label(fm55, text = loc.Msg.Not_added_abbr, relief='sunken')
    self._Setting.ScenariosSetup.WIfS_crop3.pack(side = TOP, padx = 1, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop4 = Label(fm55, text = loc.Msg.Not_added_abbr, relief='sunken')
    self._Setting.ScenariosSetup.WIfS_crop4.pack(side = TOP, padx = 1, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop5 = Label(fm55, text = loc.Msg.Not_added_abbr, relief='sunken')
    self._Setting.ScenariosSetup.WIfS_crop5.pack(side = TOP, padx = 1, pady = 5)
    fm55.pack(side = LEFT)

    # -4th frame - price
    fm56 = Frame(group52.interior())
    label54 = Label(fm56, text = 'Crop price').pack(side = TOP)
    label540 = Label(fm56, text = '(CFA/ton)').pack(side = TOP)
    self._Setting.ScenariosSetup.WIfS_crop_price1 = Pmw.EntryField(fm56, labelpos = 'w', entry_width = 10,  # self.price1
                                                                   validate={'validator': 'real', 'min': 0, 'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_crop_price1.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop_price2 = Pmw.EntryField(fm56, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0, 'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_crop_price2.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop_price3 = Pmw.EntryField(fm56, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_crop_price3.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop_price4 = Pmw.EntryField(fm56, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_crop_price4.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_crop_price5 = Pmw.EntryField(fm56, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_crop_price5.pack(side = TOP, padx = 5, pady = 5)
    fm56.pack(side = LEFT)
    # -5th frame - Price of N fertilizer
    fm57 = Frame(group52.interior())
    label55 = Label(fm57, text = 'Cost of N fert.').pack(side = TOP)
    label550 = Label(fm57, text = '(CFA/kg N)').pack(side = TOP)
    self._Setting.ScenariosSetup.WIfS_cost_Nfert1 = Pmw.EntryField(fm57, labelpos = 'w', entry_width = 10,  # self.costN1
                                                                   validate={'validator': 'real', 'min': 0.1,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_Nfert1.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_Nfert2 = Pmw.EntryField(fm57, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0.1,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_Nfert2.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_Nfert3 = Pmw.EntryField(fm57, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0.1,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_Nfert3.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_Nfert4 = Pmw.EntryField(fm57, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0.1,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_Nfert4.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_Nfert5 = Pmw.EntryField(fm57, labelpos = 'w', entry_width = 10,
                                                                   validate={'validator': 'real', 'min': 0.1,
                                                                             'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_Nfert5.pack(side = TOP, padx = 5, pady = 5)
    fm57.pack(side = LEFT)

    # -6th frame - Cost of irrigation
    fm58 = Frame(group52.interior())
    label56 = Label(fm58, text = 'Cost of irrig.').pack(side = TOP)
    label560 = Label(fm58, text = '(CFA/mm)').pack(side = TOP)
    self._Setting.ScenariosSetup.WIfS_cost_irr1 = Pmw.EntryField(fm58, labelpos = 'w', entry_width = 10,  # self.costi1
                                                                 validate={'validator': 'real', 'min': 0,
                                                                           'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_irr1.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_irr2 = Pmw.EntryField(fm58, labelpos = 'w', entry_width = 10,
                                                                 validate={'validator': 'real', 'min': 0,
                                                                           'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_irr2.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_irr3 = Pmw.EntryField(fm58, labelpos = 'w', entry_width = 10,
                                                                 validate={'validator': 'real', 'min': 0,
                                                                           'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_irr3.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_irr4 = Pmw.EntryField(fm58, labelpos = 'w', entry_width = 10,
                                                                 validate={'validator': 'real', 'min': 0,
                                                                           'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_irr4.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_cost_irr5 = Pmw.EntryField(fm58, labelpos = 'w', entry_width = 10,
                                                                 validate={'validator': 'real', 'min': 0,
                                                                           'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_cost_irr5.pack(side = TOP, padx = 5, pady = 5)
    fm58.pack(side = LEFT)

    # -7th frame - General Cost
    fm59 = Frame(group52.interior())
    label57 = Label(fm59, text = 'General Cost').pack(side = TOP)
    label570 = Label(fm59, text = '(CFA/ha)').pack(side = TOP)
    self._Setting.ScenariosSetup.WIfS_general_cost1 = Pmw.EntryField(fm59, labelpos = 'w', entry_width = 10,  # self.costG1
                                                                     validate={'validator': 'real', 'min': 0,
                                                                               'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_general_cost1.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_general_cost2 = Pmw.EntryField(fm59, labelpos = 'w', entry_width = 10,
                                                                     validate={'validator': 'real', 'min': 0,
                                                                               'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_general_cost2.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_general_cost3 = Pmw.EntryField(fm59, labelpos = 'w', entry_width = 10,
                                                                     validate={'validator': 'real', 'min': 0,
                                                                               'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_general_cost3.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_general_cost4 = Pmw.EntryField(fm59, labelpos = 'w', entry_width = 10,
                                                                     validate={'validator': 'real', 'min': 0,
                                                                               'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_general_cost4.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_general_cost5 = Pmw.EntryField(fm59,  # self.costG5
                                 labelpos = 'w', entry_width = 10,
                                                                     validate={'validator': 'real', 'min': 0,
                                                                               'minstrict': 0})
    self._Setting.ScenariosSetup.WIfS_general_cost5.pack(side = TOP, padx = 5, pady = 5)
    fm59.pack(side = LEFT)

    # -8th frame for comments
    fm591 = Frame(group52.interior())
    label47 = Label(fm591, text = ' ').pack(side = TOP)
    label370 = Label(fm591, text = 'comments').pack(side = TOP)
    self._Setting.ScenariosSetup.WIfS_comment1 = Pmw.EntryField(fm591, labelpos = 'w')
    self._Setting.ScenariosSetup.WIfS_comment1.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_comment2 = Pmw.EntryField(fm591, labelpos = 'w')
    self._Setting.ScenariosSetup.WIfS_comment2.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_comment3 = Pmw.EntryField(fm591, labelpos = 'w')
    self._Setting.ScenariosSetup.WIfS_comment3.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_comment4 = Pmw.EntryField(fm591, labelpos = 'w')
    self._Setting.ScenariosSetup.WIfS_comment4.pack(side = TOP, padx = 5, pady = 5)
    self._Setting.ScenariosSetup.WIfS_comment5 = Pmw.EntryField(fm591, labelpos = 'w')
    self._Setting.ScenariosSetup.WIfS_comment5.pack(side = TOP, padx = 5, pady = 5)
    fm591.pack(side = LEFT)

    # init. Error Message Dialogs
    self.__initMessageDialogs()

    # # 2. Visualize DSSAT output
    group54 = Pmw.Group(sf_p5.interior(), tag_text = 'Display Outputs')
    group54.pack(fill = 'x', expand = 1, anchor = N, padx = 10, pady = 5)
    # button52 = tkinter.Button(group54.interior(), text = 'Run DSSAT for N weather realizations',
    #                           command = self.RunSIMAGRI, bg = 'green').pack(fill = 'x', expand = 1, side = TOP, anchor = N,
    #                                                                       padx = 10, pady = 5)

    self._Setting.ScenariosSetup.comp_year = Pmw.EntryField(group54.interior(), # self.wsi_threshold
                                        labelpos = 'w', label_text = 'A distinctive year to compare with climatology ',
                                        value='2002',
                                        validate = {'validator': 'numeric', 'min' : 1981, 'max' : 2018, 'minstrict' : 0})
    self._Setting.ScenariosSetup.comp_year.pack(side = TOP, padx = 5, pady = 5)

    # Create button to execute graphs (1)yield boxplot
    fm592 = Frame(group54.interior())
    button53 = tkinter.Button(fm592, text = 'I. Yield Boxplot',
                              command = self.Yield_boxplot_hist, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP, anchor = N,
                                                                                                                        padx = 10, pady = 5)                                                                             
    # Create button to execute graphs (3) water stress index
    button55 = tkinter.Button(fm592, text = 'III. Water Stress',
                              command = self.WSGD_plot_hist, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP, anchor = N,
                                                                                 padx = 10, pady = 5)
    # Create button to execute graphs (5) gross margin
    button57 = tkinter.Button(fm592, text = 'V. Gross Margin (Boxplot)',
                              command = self.Gross_margin_box, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP,
                                                                                      anchor = N, padx = 10, pady = 5)
    # Create button to execute graphs (7) weather variables(Tmin, Tmax, Srad)
    button59 = tkinter.Button(fm592, text = 'VII. Weather (Tmin, Tmax, Srad)',
                              command = self.weather_plot_hist, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP,
                                                                                      anchor = N, padx = 10, pady = 5)
    fm592.pack(fill = 'x', expand = 1, side = LEFT)

    # Create button to execute graphs (2) yield exceedance curve
    fm593 = Frame(group54.interior())
    button54 = tkinter.Button(fm593, text = 'II. Yield Exceedance Curve',
                              command = self.Yield_exceedance, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP, anchor = N,
                                                                                   padx = 10, pady = 5)          
    # Create button to execute graphs (4) Nitrogen stress
    button56 = tkinter.Button(fm593, text = 'IV. Nitrogen Stress(SCF & climatology)',
                              command = self.NSTD_plot_hist, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP, anchor = N,
                                                                                padx = 10, pady = 5)
    # Create button to execute graphs (6) gross margin exceedance curve
    button58 = tkinter.Button(fm593, text = 'VI. Gross Margin (Exceedance Curve)',
                              command = self.Gross_margin_exd, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP,
                                                                                    anchor = N, padx = 10, pady = 5)
    # Create button to execute graphs (8)weather variables(cumulative rainfall)
    button60 = tkinter.Button(fm593, text = 'VIII. Weather (Cumulative Rainfall)',
                              command = self.cum_rain_plot_hist, bg = 'peachpuff1').pack(fill = 'x', expand = 1, side = TOP,
                                                                                    anchor = N, padx = 10, pady = 5)
    fm593.pack(fill = 'x', expand = 1, side = LEFT)

    # # 3. Visualize DSSAT output (time-series)
    group55 = Pmw.Group(sf_p5.interior(), tag_text = 'Display Outputs (TIME-SERIES)')
    group55.pack(fill = 'x', expand = 1, anchor = N, padx = 10, pady = 5)
    button_T1 = tkinter.Button(group55.interior(), text = 'Yield Time-series',
                              command = self.Yield_tseries_hist, bg = 'PaleGreen').pack(fill = 'x', expand = 1, side = TOP, anchor = N,
                                                                                                                        padx = 10, pady = 5)  
  updateNeeded = False
  def updateGUI(self):
    while self.updateNeeded:
      root.update()
      sleep(0.1)


  def __initMessageDialogs(self):
    self.working_directory_err = Pmw.MessageDialog(self._UIParent, title = 'Error message in Working Directory',
                                       defaultbutton = 0,
                                       message_text = 'No working directory available!')
    self.working_directory_err.withdraw()

    self.make_working_directory_error = Pmw.MessageDialog(self._UIParent, title = 'Error message in copying SIMAGRI module',
                                           defaultbutton = 0,
                                           message_text = "Can't make working directory due to permission denied. \n Please change other working directory")
    self.make_working_directory_error.withdraw()
    self.copy_camdt_module_error = Pmw.MessageDialog(self._UIParent, title = 'Error message in copying SIMAGRI module',
                                           defaultbutton = 0,
                                           message_text = "Can't copy SIMAGRI modules to working directory due to permission denied. \n Please change other working directory")
    self.copy_camdt_module_error.withdraw()

    self.disease_model_output_err = Pmw.MessageDialog(self._UIParent, title = 'Error in Rice Disease Risk',
                                       defaultbutton = 0,
                                       message_text = 'No Rice Disease Risk output ! Please enable/run Disease Model on the Disease setup page')
    self.disease_model_output_err.withdraw()

    self.rpath_err = Pmw.MessageDialog(self._UIParent, title='Error message in Run DSSAT for n weather realizations',
                                       defaultbutton = 0,
                                       message_text='R program does not exist in "RScript path" of DISEASE Setup.')
    self.rpath_err.withdraw()

    # error message when there is no scenario name
    self.writeSNX_err = Pmw.MessageDialog(self._UIParent, title = 'Error message in writing SNX.txt',
                                       defaultbutton = 0,
                                       message_text = 'No scenario name available!')
    self.writeSNX_err.withdraw()

    # error message when fertilizer info is missing
    self.fertilizer_err = Pmw.MessageDialog(self._UIParent, title = 'Error in Fertilizer Input',
                                       defaultbutton = 0,
                                       message_text = 'No Fertilizer input! Please click the Button for more detiled input on DSSAT setup2 page.')
    self.fertilizer_err.withdraw()
    # error message when cultivar info is missing
    self.cultivar_err = Pmw.MessageDialog(self._UIParent, title = 'Error in Cultivar Input',
                                     defaultbutton = 0,
                                     message_text = 'No Cultivar input! Please click the Button for more detiled input on DSSAT setup1 page.')
    self.cultivar_err.withdraw()
    # error message when irrigation info is missing
    self.irrigation_err = Pmw.MessageDialog(self._UIParent, title = 'Error in Irrigation Input',
                                       defaultbutton = 0,
                                       message_text = 'No Irrigaiotn input! Please click the Button for more detiled input on DSSAT setup2 page.')
    self.irrigation_err.withdraw()

    # error message when there is no number of fertilizer applications
    self.nfert_err_dialog = Pmw.MessageDialog(self._UIParent, title = 'Error in fertilizer input',
                                         defaultbutton = 0,
                                         message_text = 'Number of fertilizer applications should be provided in "DSSAT setup 2" tab!')
    self.nfert_err_dialog.iconname('Error in fertilizer input')
    self.nfert_err_dialog.withdraw()

    # error message when there is no number of irrigation (for only irr option=on reported dates)
    self.nirr_err_dialog = Pmw.MessageDialog(self._UIParent, title = 'Error in fertilizer input',
                                        defaultbutton = 0,
                                        message_text = 'Number of irrigations should be provided if "On Reported dates" is chosen in "DSSAT setup 2" tab!')
    self.nirr_err_dialog.iconname('Error in irrigation input')
    self.nirr_err_dialog.withdraw()

    # error message when there is no scenario name
    self.Eanalysis_err = Pmw.MessageDialog(self._UIParent, title = 'Error in Economical Analysis',
                                           defaultbutton = 0,
                                           message_text = 'Cost/price info is missing. Please fill out those columns for economic analysis!')
    self.Eanalysis_err.withdraw()

    # error message when there is no scenario name
    self.Run_DSSAT_err = Pmw.MessageDialog(self._UIParent, title = 'Error in Run DSSAT for N weather realizations',
                                           defaultbutton = 0,
                                           message_text = 'No Scenarios output!')
    self.Run_DSSAT_err.withdraw()

    # error message when this is no scenario params.
    self.run_dssat_no_params = Pmw.MessageDialog(self._UIParent, title = 'Error in Run DSSAT for N weather realizations',
                                           defaultbutton = 0,
                                           message_text = 'No Scenarios Parameters!')
    self.run_dssat_no_params.withdraw()

    self.run_dssat_rewrite_params = Pmw.MessageDialog(self._UIParent, title = 'Error in Run DSSAT for N weather realizations',
                                           defaultbutton = 0,
                                           message_text = 'Rewrite Parameters after running!')
    self.run_dssat_rewrite_params.withdraw()

    # error message when there is no scenario name
    self.Eanalysis_err = Pmw.MessageDialog(self._UIParent, title = 'Error in Economical Analysis',
                                           defaultbutton = 0,
                                           message_text = 'Cost/price info is missing. Please fill out those columns for economic analysis!')
    self.Eanalysis_err.withdraw()
  #=================================================================
  # Run DSSAT based on SCF
  # by doing 1)write SNX, 2)write V47, 3)run temporal downscaling, 4) run DSSAT 100 times
  #=================================================================
  # def runDSSAT(self, sname, SCF_flag, sce_no):
  def runDSSAT(self, sname, sce_no):
    if sname == "":
      self._ModErrMsg.writeSNX_err.activate()
    else:
      # sname = self._Setting.ScenariosSetup.WIfS_name1.getvalue()
      if self._Setting.DSSATSetup1.Crop_type.get() == 0:  #Peanut
        SNX_fname = path.join(self._Setting.ScenariosSetup.Working_directory, "SEPN"+sname+".SNX")
        cr_type='PN'
        # plt_doy = self._Setting.DSSATSetup1.PN_plt_date.getvalue()
        if sce_no == 1:
          self._Setting.ScenariosSetup.WIfS_crop1.configure(text = 'PN', background = 'honeydew1')
        elif sce_no == 2:
          self._Setting.ScenariosSetup.WIfS_crop2.configure(text = 'PN', background = 'honeydew1')
        elif sce_no == 3:
          self._Setting.ScenariosSetup.WIfS_crop3.configure(text = 'PN', background = 'honeydew1')
        elif sce_no == 4:
          self._Setting.ScenariosSetup.WIfS_crop4.configure(text = 'PN', background = 'honeydew1')
        else:
          self._Setting.ScenariosSetup.WIfS_crop5.configure(text = 'PN', background = 'honeydew1')
      elif self._Setting.DSSATSetup1.Crop_type.get() == 1:  #Millet
        SNX_fname = path.join(self._Setting.ScenariosSetup.Working_directory, "SEML"+sname+".SNX")
        cr_type='ML'
        # plt_doy = self._Setting.DSSATSetup1.ML_plt_date.getvalue()
        if sce_no == 1:
          self._Setting.ScenariosSetup.WIfS_crop1.configure(text = 'ML', background = 'honeydew1')
        elif sce_no == 2:
          self._Setting.ScenariosSetup.WIfS_crop2.configure(text = 'ML', background = 'honeydew1')
        elif sce_no == 3:
          self._Setting.ScenariosSetup.WIfS_crop3.configure(text = 'ML', background = 'honeydew1')
        elif sce_no == 4:
          self._Setting.ScenariosSetup.WIfS_crop4.configure(text = 'ML', background = 'honeydew1')
        else:
          self._Setting.ScenariosSetup.WIfS_crop5.configure(text = 'ML', background = 'honeydew1')
      else:  #sorghum
        SNX_fname = path.join(self._Setting.ScenariosSetup.Working_directory, "SESG"+sname+".SNX")
        cr_type='SG'
        # plt_doy = self._Setting.DSSATSetup1.SG_plt_date.getvalue()
        if sce_no == 1:
          self._Setting.ScenariosSetup.WIfS_crop1.configure(text = 'SG', background = 'honeydew1')
        elif sce_no == 2:
          self._Setting.ScenariosSetup.WIfS_crop2.configure(text = 'SG', background = 'honeydew1')
        elif sce_no == 3:
          self._Setting.ScenariosSetup.WIfS_crop3.configure(text = 'SG', background = 'honeydew1')
        elif sce_no == 4:
          self._Setting.ScenariosSetup.WIfS_crop4.configure(text = 'SG', background = 'honeydew1')
        else:
          self._Setting.ScenariosSetup.WIfS_crop5.configure(text = 'SG', background = 'honeydew1')
      #check if observed weather is avilable until harvest
      plt_year = self._Setting.DSSATSetup1.sim_year1.getvalue()
      temp = plt_year + '-' + self._Setting.DSSATSetup1.plt_month.getvalue() + '-' + self._Setting.DSSATSetup1.plt_date.getvalue()
      plt_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
      planting = int(plt_year+repr(plt_doy).zfill(3))
      harvesting = planting + 170  #crop growing period approximately 170 assumed
      #adjust hv_date if harvest moves to a next year
      yr_end_date = 365
      if calendar.isleap(int(self._Setting.DSSATSetup1.sim_year2.getvalue())): yr_end_date = 366
      if harvesting%1000 > yr_end_date:
        harvesting = harvesting - yr_end_date + 1000     

      # # plt_year = self._Setting.DSSATSetup1.plt_year.getvalue()
      # temp = plt_year+'-'+self._Setting.DSSATSetup1.plt_month.getvalue()+'-'+self._Setting.DSSATSetup1.plt_date.getvalue()
      # plt_doy = datetime.datetime.strptime(temp, '%Y-%m-%d').timetuple().tm_yday
      # planting = int(plt_year+repr(plt_doy).zfill(3))
      # # harvesting = planting + 180
      # harvesting = planting + 130  #EJ(4/28/2020: peanut groriwng period is approximately 3 months due to the limited rain (short rainy season))
      # #adjust hv_date if harvest moves to a next year
      # yr_end_date = 365
      # if calendar.isleap(int(plt_year)): yr_end_date = 366
      # if harvesting%1000 > yr_end_date:
      #   harvesting = harvesting - yr_end_date + 1000

      # WSTA = self._Setting.DSSATSetup1.WStation.getvalue()[0][0:4] #'UYTT'
      # WTD_fname = path.join(self._Setting.ScenariosSetup.Working_directory, WSTA + ".WTD")
      # WTD_last_date = find_obs_date(WTD_fname)  #WTD_last_date [yyyydoy] in int format
      # if WTD_last_date >= harvesting: #180 is arbitrary number to fully cover weather data until late maturity
      #   obs_flag = 1
      # else:
      #   obs_flag = 0

      #=================================================================
      # Run DSSAT based on historical weather observations
      # by doing 1)write SNX, 2)write V47, 3) run DSSAT for n historical years
      #===============================================================
      #0) Write scenario summary into a text file just for reference
       # + Check fertilizer input by running writeSNX in simagrimodule.py
      self._SIMAGRIProc.write_scen_summary(sname)
      #1) wirte SNX
      self._SIMAGRIProc.writeSNX_main_hist(SNX_fname, cr_type)
      #2) write V47 file
      self._SIMAGRIProc.writeV47_main_hist(SNX_fname, cr_type)
      #3) Run DSSAT based on n years of historical observations
      entries = ("PlantGro.OUT","Evaluate.OUT", 
                "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
              "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
      #==RUN DSSAT with ARGUMENT 
      Wdir_path = self._Setting.ScenariosSetup.Working_directory
      os.chdir(Wdir_path)  #change directory  #check if needed o rnot
      if cr_type == 'SG':
        args = "DSCSM047.EXE SGCER047 B DSSBatch.v47"
      elif cr_type == 'PN':
        args = "DSCSM047.EXE CRGRO047 B DSSBatch.v47"
      else:
        args = "DSCSM047.EXE MLCER047 B DSSBatch.v47"
      subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
      print('Done DSSAT-historical runs!  ')
      # print('It took {0:5.1f} sec to finish all steps to from write_SNX, downscaling, DSSAT runs (forecast & historical) '.format(end_time0-start_time))
      print('<<==================================================================>>')      
      print('<<===CHECK SIMULATION RESULTS BY CLICKING THE APPROPRIATE BUTTONS====>>')   
      print('<<==================================================================>>')     
      #7) Save DSSAT output into a new folder        
      #create a new folder to save outputs of the target scenario
      new_folder=sname+"_output_hist"
      if os.path.exists(new_folder):
          shutil.rmtree(new_folder)   #remove existing folder
      os.makedirs(new_folder)
      #copy outputs to the new folder
      dest_dir=path.join(self._Setting.ScenariosSetup.Working_directory, new_folder)
      for entry in entries:
        if os.path.isfile(entry):
          shutil.move(entry, dest_dir)
        else:
            print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')
      #move *.SNX file to the new folder with output files
      if os.path.isfile(SNX_fname):
        shutil.move(SNX_fname, dest_dir)
      summary_fname = path.join(self._Setting.ScenariosSetup.Working_directory, sname+"_summary.txt") 
      if os.path.isfile(summary_fname):
        shutil.move(summary_fname, dest_dir)
  def Yield_boxplot_hist(self):
    # Get output YIELD.txt from all scenario
    # fname = []
    fname_hist = []
    scename = []  # scenario name
    cr_list = []
    temp_entries = [[self._Setting.ScenariosSetup.WIfS_crop1.cget("text"), self._Setting.ScenariosSetup.WIfS_name1.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop2.cget("text"), self._Setting.ScenariosSetup.WIfS_name2.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop3.cget("text"), self._Setting.ScenariosSetup.WIfS_name3.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop4.cget("text"), self._Setting.ScenariosSetup.WIfS_name4.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop5.cget("text"), self._Setting.ScenariosSetup.WIfS_name5.getvalue()]]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    for entry in temp_entries:
      if entry[0] != loc.Msg.Not_added_abbr and len(entry[1]) == 4:
        # fname.append(path.join(Wdir_path, entry[1] +"_output", "Summary.out"))
        fname_hist.append(path.join(Wdir_path, entry[1] +"_output_hist", "Summary.out"))
        scename.append(entry[1])
        cr_list.append(entry[0])

    # self._DISPLAY_Proc.Yield_boxplot_hist(fname, fname_hist, scename) #, cr_list)
    self._DISPLAY_Proc.Yield_boxplot_hist(fname_hist, scename) #, cr_list)

  def Yield_exceedance(self):
    # Get output YIELD.txt from all scenario
    fname = []
    scename = []  # scenario name
    cr_list = []
    temp_entries = [[self._Setting.ScenariosSetup.WIfS_crop1.cget("text"), self._Setting.ScenariosSetup.WIfS_name1.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop2.cget("text"), self._Setting.ScenariosSetup.WIfS_name2.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop3.cget("text"), self._Setting.ScenariosSetup.WIfS_name3.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop4.cget("text"), self._Setting.ScenariosSetup.WIfS_name4.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop5.cget("text"), self._Setting.ScenariosSetup.WIfS_name5.getvalue()]]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    for entry in temp_entries:
      if entry[0] != loc.Msg.Not_added_abbr and len(entry[1]) == 4:
        fname.append(path.join(Wdir_path, entry[1] + "_output_hist", "Summary.out"))
        scename.append(entry[1])
        cr_list.append(entry[0])

    self._DISPLAY_Proc.Yield_exceedance(fname, scename) #, cr_list)

  #=================================================================================
  def WSGD_plot_hist(self):
    # Get output YIELD.txt from all scenario
    # fname = []
    fname_hist = []
    scename = []  # scenario name
    cr_list = []
    temp_entries = [[self._Setting.ScenariosSetup.WIfS_crop1.cget("text"), self._Setting.ScenariosSetup.WIfS_name1.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop2.cget("text"), self._Setting.ScenariosSetup.WIfS_name2.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop3.cget("text"), self._Setting.ScenariosSetup.WIfS_name3.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop4.cget("text"), self._Setting.ScenariosSetup.WIfS_name4.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop5.cget("text"), self._Setting.ScenariosSetup.WIfS_name5.getvalue()]]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    # obs_flag_list = []
    for entry in temp_entries:
      if entry[0] != loc.Msg.Not_added_abbr and len(entry[1]) == 4:
        # fname.append(path.join(Wdir_path, entry[1] +"_output", "PlantGro.OUT"))
        fname_hist.append(path.join(Wdir_path, entry[1] +"_output_hist", "PlantGro.OUT"))
        scename.append(entry[1])
        cr_list.append(entry[0])
        # fout = path.join(Wdir_path, entry[1] +"_output", "Summary.out")
        # df_OUT=pd.read_csv(fout,delim_whitespace=True ,skiprows=3)
        # RUNNO = df_OUT.iloc[:,0].values 
        # if RUNNO[-1] == 101:  #if last line is from the observed weather
        #   obs_flag_list.append(1)
        # else:
        #   obs_flag_list.append(0)

    self._DISPLAY_Proc.WSGD_plot_hist(fname_hist, scename) #, obs_flag_list) #, cr_list)

  def NSTD_plot_hist(self):
    # Get output YIELD.txt from all scenario
    # fname = []
    fname_hist = []
    scename = []  # scenario name
    cr_list = []
    temp_entries = [[self._Setting.ScenariosSetup.WIfS_crop1.cget("text"), self._Setting.ScenariosSetup.WIfS_name1.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop2.cget("text"), self._Setting.ScenariosSetup.WIfS_name2.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop3.cget("text"), self._Setting.ScenariosSetup.WIfS_name3.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop4.cget("text"), self._Setting.ScenariosSetup.WIfS_name4.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop5.cget("text"), self._Setting.ScenariosSetup.WIfS_name5.getvalue()]]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    obs_flag_list = []
    for entry in temp_entries:
      if entry[0] != loc.Msg.Not_added_abbr and len(entry[1]) == 4:
        # fname.append(path.join(Wdir_path, entry[1] +"_output", "PlantGro.OUT"))
        fname_hist.append(path.join(Wdir_path, entry[1] +"_output_hist", "PlantGro.OUT"))
        scename.append(entry[1])
        cr_list.append(entry[0])
        # fout = path.join(Wdir_path, entry[1] +"_output", "Summary.out")
        # df_OUT=pd.read_csv(fout,delim_whitespace=True ,skiprows=3)
        # RUNNO = df_OUT.iloc[:,0].values 
        # if RUNNO[-1] == 101:  #if last line is from the observed weather
        #   obs_flag_list.append(1)
        # else:
        #   obs_flag_list.append(0)

    self._DISPLAY_Proc.NSTD_plot_hist(fname_hist, scename) #, obs_flag_list) #, cr_list)
  #=================================================================================
  def Gross_margin_box(self):  #boxplot of gross margin
    #Read a distinctive year to compare => EJ(11/30/2020)
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_GMargin = []

    # Get output YIELD.txt from all scenario
    # cr_list = []
    fname=[]
    scename=[]   #scenario name
    costN_list=[]
    costI_list=[]
    costG_list=[]
    price_list=[]
    temp_entries = [
      [
        self._Setting.ScenariosSetup.WIfS_name1.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price1.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert1.getvalue(), self._Setting.ScenariosSetup.WIfS_cost_irr1.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost1.getvalue(), self._Setting.ScenariosSetup.WIfS_crop1.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name2.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price2.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert2.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr2.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost2.getvalue(), self._Setting.ScenariosSetup.WIfS_crop2.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name3.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price3.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert3.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr3.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost3.getvalue(), self._Setting.ScenariosSetup.WIfS_crop3.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name4.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price4.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert4.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr4.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost4.getvalue(), self._Setting.ScenariosSetup.WIfS_crop4.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name5.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price5.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert5.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr5.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost5.getvalue(), self._Setting.ScenariosSetup.WIfS_crop5.cget("text")
      ]
    ]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    for entry in temp_entries:
      if entry[0] != '':
        if entry[1] == '' or entry[5] == loc.Msg.Not_added_abbr:
          self.Eanalysis_err.activate()
          # pass
        else:
          costN_list.append(entry[2])
          costI_list.append(entry[3])
          costG_list.append(entry[4])
          price_list.append(entry[1])
          scename.append(entry[0])
          count=count+1
          scenario_name = entry[0]
          fname.append(path.join(Wdir_path, entry[0] +"_output_hist", "Summary.out"))
          # cr_list.append(entry[5])

    #create an empty matrix
    nrealiz = 70  #arbitrary
    GMargin_data = np.empty([nrealiz,count])*np.nan
    HWAM_n = np.empty([nrealiz,count])*np.nan
    NICM_n = np.empty([nrealiz,count])*np.nan
    IRCM_n = np.empty([nrealiz,count])*np.nan
    # HWAM_obs = []
    # NICM_obs = []
    # IRCM_obs = []
    # GMargin_obs = []
    # obs_flag_list = []
    max_nyear = 1
    #Read Summary.out from all scenario output
    for x in range(0, count):
      fr = open(fname[x],"r") #opens summary.out to read
      price=price_list[x]   #$/ton
      cost_N=costN_list[x] #$/kg N
      cost_I=costI_list[x] #$/mm irrigation cost
      cost_G=costG_list[x] #$/ha general cost
      df_OUT=pd.read_csv(fname[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  # yield at maturity
      NICM = df_OUT.iloc[:,40].values  #read 40th column only,  #NICM   Tot N app kg/ha Inorganic N applied (kg [N]/ha) 
      IRCM = df_OUT.iloc[:,31].values    #IRCM   Irrig mm        Season irrigation (mm)  

      HWAM_n[0:len(HWAM),x] = HWAM
      NICM_n[0:len(HWAM),x] = NICM
      IRCM_n[0:len(HWAM),x] = IRCM

      #Read a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        PDAT = df_OUT.iloc[:,14].values  #read 14th column only
        doy = repr(PDAT[0])[4:]
        target = repr(target_yr) + doy
        yr_index = np.argwhere(PDAT == int(target))
        TG_yield = HWAM[yr_index[0][0]]
        TG_yield = int(TG_yield) if TG_yield >= 0 else 0  # if HWAM == -99, consider it as "0" yield (i.e., crop failure)
        # TG_yield[TG_yield < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
        temp_margin = TG_yield*float(price)*0.001 - float(cost_N)*NICM[yr_index[0][0]] - float(cost_I)*IRCM[yr_index[0][0]] - float(cost_G) #$/ha
        TG_GMargin.append(temp_margin)

      HWAM_n[HWAM_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
      #Compute gross margin
      GMargin_data[0:len(HWAM),x]=HWAM_n[0:len(HWAM),x]*float(price)*0.001 - float(cost_N)*NICM_n[0:len(HWAM),x] - float(cost_I)*IRCM_n[0:len(HWAM),x] - float(cost_G) #$/ha

    #trim yield_hist so as to make boxplot with variable length of data => use Just use a list of arrays or lists
    filtered_GMargin= []
    for x in range(0, count):
      temp = GMargin_data[:,x]
      filtered_GMargin.append(temp[~np.isnan(temp)])

    # GMargin_data = GMargin_data[0:max_nyear,:]

    #X data for plot
    myXList=[i+1 for i in range(count)]

    #Plotting
    fig = plt.figure()
    fig.suptitle('Gross Margin', fontsize=12, fontweight='bold')

    ax = fig.add_subplot(111)
    ax.set_xlabel('Scenario',fontsize=12)
    ax.set_ylabel('Gross Margin[US$/ha]',fontsize=12)

    if yr_flag == 1:  #=> EJ(11/30/2020)
      ax.plot(myXList, TG_GMargin, 'ro', label='selected year') #'ro-')
      ax.legend(loc='upper left')   

    # ax.boxplot(GMargin_data,labels=scename, showmeans=True, meanline=True, notch=True) #, bootstrap=10000)
    ax.boxplot(filtered_GMargin,labels=scename, showmeans=True, meanline=True, notch=True) 
    # fig_name = Wdir_path + "\\"+ scename[0] +"_output\\grossmargin_box.png"
    # plt.savefig(fig_name)
    plt.show()

  
  def Gross_margin_exd(self):
    #Read a distinctive year to compare => EJ(11/30/2020)
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_GMargin = []

    # Get output YIELD.txt from all scenario
    # cr_list = []
    fname=[]
    scename=[]   #scenario name
    costN_list=[]
    costI_list=[]
    costG_list=[]
    price_list=[]
    temp_entries = [
      [
        self._Setting.ScenariosSetup.WIfS_name1.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price1.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert1.getvalue(), self._Setting.ScenariosSetup.WIfS_cost_irr1.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost1.getvalue(), self._Setting.ScenariosSetup.WIfS_crop1.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name2.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price2.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert2.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr2.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost2.getvalue(), self._Setting.ScenariosSetup.WIfS_crop2.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name3.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price3.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert3.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr3.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost3.getvalue(), self._Setting.ScenariosSetup.WIfS_crop3.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name4.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price4.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert4.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr4.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost4.getvalue(), self._Setting.ScenariosSetup.WIfS_crop4.cget("text")
      ],
      [
        self._Setting.ScenariosSetup.WIfS_name5.getvalue(), self._Setting.ScenariosSetup.WIfS_crop_price5.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_Nfert5.getvalue(),
        self._Setting.ScenariosSetup.WIfS_cost_irr5.getvalue(),
        self._Setting.ScenariosSetup.WIfS_general_cost5.getvalue(), self._Setting.ScenariosSetup.WIfS_crop5.cget("text")
      ]
    ]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    for entry in temp_entries:
      if entry[0] != '':
        if entry[1] == '' or entry[5] == loc.Msg.Not_added_abbr:
          self.Eanalysis_err.activate()
          # pass
        else:
          costN_list.append(entry[2])
          costI_list.append(entry[3])
          costG_list.append(entry[4])
          price_list.append(entry[1])
          scename.append(entry[0])
          count=count+1
          scenario_name = entry[0]
          fname.append(path.join(Wdir_path, entry[0] +"_output_hist", "Summary.out"))
          # cr_list.append(entry[5])

    #create an empty matrix
    nrealiz = 70  #arbitrary
    GMargin_data = np.empty([nrealiz,count])*np.nan
    HWAM_n = np.empty([nrealiz,count])*np.nan
    NICM_n = np.empty([nrealiz,count])*np.nan
    IRCM_n = np.empty([nrealiz,count])*np.nan
    HWAM_obs = []
    NICM_obs = []
    IRCM_obs = []
    GMargin_obs = []
    obs_flag_list = []
    #Read Summary.out from all scenario output
    for x in range(0, count):
      fr = open(fname[x],"r") #opens summary.out to read
      price=price_list[x]   #$/ton
      cost_N=costN_list[x] #$/kg N
      cost_I=costI_list[x] #$/mm irrigation cost
      cost_G=costG_list[x] #$/ha general cost
      df_OUT=pd.read_csv(fname[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  # yield at maturity
      NICM = df_OUT.iloc[:,40].values  #read 40th column only,  #NICM   Tot N app kg/ha Inorganic N applied (kg [N]/ha) 
      IRCM = df_OUT.iloc[:,31].values    #IRCM   Irrig mm        Season irrigation (mm)  

      HWAM_n[0:len(HWAM),x] = HWAM
      NICM_n[0:len(HWAM),x] = NICM
      IRCM_n[0:len(HWAM),x] = IRCM

      #Read a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        PDAT = df_OUT.iloc[:,14].values  #read 14th column only
        doy = repr(PDAT[0])[4:]
        target = repr(target_yr) + doy
        yr_index = np.argwhere(PDAT == int(target))
        TG_yield = HWAM[yr_index[0][0]]
        TG_yield = int(TG_yield) if TG_yield >= 0 else 0  # if HWAM == -99, consider it as "0" yield (i.e., crop failure)
        # TG_yield[TG_yield < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
        temp_margin = TG_yield*float(price)*0.001 - float(cost_N)*NICM[yr_index[0][0]] - float(cost_I)*IRCM[yr_index[0][0]] - float(cost_G) #$/ha
        TG_GMargin.append(temp_margin)

      HWAM_n[HWAM_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
      #Compute gross margin
      GMargin_data[0:len(HWAM),x]=HWAM_n[0:len(HWAM),x]*float(price)*0.001 - float(cost_N)*NICM_n[0:len(HWAM),x] - float(cost_I)*IRCM_n[0:len(HWAM),x] - float(cost_G) #$/ha
        
    # Plotting yield exceedance curve
    fig = plt.figure()
    fig.suptitle('Gross Margin Exceedance Curve', fontsize=12, fontweight='bold')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Gross Margin[CFA/ha]',fontsize=14)
    ax.set_ylabel('Probability of Exceedance [-]',fontsize=14)
    plt.ylim(0, 1) 
    for x in range(count):
      #count n-th day when all n-years values are available
      nyear = np.sum(~np.isnan(GMargin_data[:,x]), axis = 0) #EJ(11/30/2020)
      sorted_gmargin = np.sort(GMargin_data[0:nyear,x])
      fx_scf = [1.0/len(sorted_gmargin)] * len(sorted_gmargin) #pdf
      Fx_scf = 1.0-np.cumsum(fx_scf)  #for exceedance curve
      ax.plot(sorted_gmargin,Fx_scf,'o-', label=scename[x])
      #vertical line for the yield with observed weather
      if yr_flag == 1:
        x_data=[TG_GMargin[x],TG_GMargin[x]] #only two points to draw a line
        y_data=[0,1]
        temp= repr(target_yr)[2:] + '-' + scename[x]
        ax.plot(x_data,y_data,'-.', label=temp)
        plt.ylim(0, 1)

    box = ax.get_position()  # Shrink current axis by 15%
    ax.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    # fig_name = Wdir_path + "\\" + sname + "_output\\" + sname +"_Yield_exceedance.png"
    # plt.savefig(fig_name)  
    plt.show()

  
  def weather_plot_hist(self):
    # Get output YIELD.txt from all scenario
    # fname = []
    fname_hist = []
    scename = []  # scenario name
    cr_list = []
    temp_entries = [[self._Setting.ScenariosSetup.WIfS_crop1.cget("text"), self._Setting.ScenariosSetup.WIfS_name1.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop2.cget("text"), self._Setting.ScenariosSetup.WIfS_name2.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop3.cget("text"), self._Setting.ScenariosSetup.WIfS_name3.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop4.cget("text"), self._Setting.ScenariosSetup.WIfS_name4.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop5.cget("text"), self._Setting.ScenariosSetup.WIfS_name5.getvalue()]]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    obs_flag_list = []
    for entry in temp_entries:
      if entry[0] != loc.Msg.Not_added_abbr and len(entry[1]) == 4:
        # fname.append(path.join(Wdir_path, entry[1] +"_output", "Weather.OUT"))
        fname_hist.append(path.join(Wdir_path, entry[1] +"_output_hist", "Weather.OUT"))
        scename.append(entry[1])
        cr_list.append(entry[0])
        # fout = path.join(Wdir_path, entry[1] +"_output", "Summary.out")
        # df_OUT=pd.read_csv(fout,delim_whitespace=True ,skiprows=3)
        # RUNNO = df_OUT.iloc[:,0].values 
        # if RUNNO[-1] == 101:  #if last line is from the observed weather
        #   obs_flag_list.append(1)
        # else:
        #   obs_flag_list.append(0)

    self._DISPLAY_Proc.weather_plot_hist(fname_hist, scename) #, obs_flag_list) #, cr_list)

  def cum_rain_plot_hist(self):
    # Get output YIELD.txt from all scenario
    # fname = []
    fname_hist = []
    scename = []  # scenario name
    cr_list = []
    temp_entries = [[self._Setting.ScenariosSetup.WIfS_crop1.cget("text"), self._Setting.ScenariosSetup.WIfS_name1.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop2.cget("text"), self._Setting.ScenariosSetup.WIfS_name2.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop3.cget("text"), self._Setting.ScenariosSetup.WIfS_name3.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop4.cget("text"), self._Setting.ScenariosSetup.WIfS_name4.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop5.cget("text"), self._Setting.ScenariosSetup.WIfS_name5.getvalue()]]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    obs_flag_list = []
    for entry in temp_entries:
      if entry[0] != loc.Msg.Not_added_abbr and len(entry[1]) == 4:
        # fname.append(path.join(Wdir_path, entry[1] +"_output", "Weather.OUT"))
        fname_hist.append(path.join(Wdir_path, entry[1] +"_output_hist", "Weather.OUT"))
        scename.append(entry[1])
        cr_list.append(entry[0])
        # fout = path.join(Wdir_path, entry[1] +"_output", "Summary.out")
        # df_OUT=pd.read_csv(fout,delim_whitespace=True ,skiprows=3)
        # RUNNO = df_OUT.iloc[:,0].values 
        # if RUNNO[-1] == 101:  #if last line is from the observed weather
        #   obs_flag_list.append(1)
        # else:
        #   obs_flag_list.append(0)

    self._DISPLAY_Proc.cum_rain_plot_hist(fname_hist, scename)#, obs_flag_list) #, cr_list)

  def Yield_tseries_hist(self):
    fname_hist = []
    scename = []  # scenario name
    cr_list = []
    temp_entries = [[self._Setting.ScenariosSetup.WIfS_crop1.cget("text"), self._Setting.ScenariosSetup.WIfS_name1.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop2.cget("text"), self._Setting.ScenariosSetup.WIfS_name2.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop3.cget("text"), self._Setting.ScenariosSetup.WIfS_name3.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop4.cget("text"), self._Setting.ScenariosSetup.WIfS_name4.getvalue()],
                    [self._Setting.ScenariosSetup.WIfS_crop5.cget("text"), self._Setting.ScenariosSetup.WIfS_name5.getvalue()]]
    count = 0
    Wdir_path = self._Setting.ScenariosSetup.Working_directory
    for entry in temp_entries:
      if entry[0] != loc.Msg.Not_added_abbr and len(entry[1]) == 4:
        # fname.append(path.join(Wdir_path, entry[1] +"_output", "Summary.out"))
        fname_hist.append(path.join(Wdir_path, entry[1] +"_output_hist", "Summary.out"))
        scename.append(entry[1])
        cr_list.append(entry[0])
        
    self._DISPLAY_Proc.Yield_tseries_hist(fname_hist, scename) #, cr_list)
#==============================================================================
# Select working directory to run SIMAGRI fortran executable
  def getWdir(self):

    # Use current run HDD as initial directory if OS is WindowsOS
    initialdir = "/"
    if WindowsOS == True:
      initialdir = os.getcwd()[0] + ":\\"

    self._Setting.ScenariosSetup.Working_directory = askdirectory(initialdir = initialdir, title = "Select working dir")
    self.WDir_label.configure(text = self._Setting.ScenariosSetup.Working_directory)
    self.WDir_label.configure(background = 'lavenderblush1')


  def copyModule(self):
    if self._Setting.ScenariosSetup.Working_directory is None \
        or self._Setting.ScenariosSetup.Working_directory == '' \
        or self._Setting.ScenariosSetup.Working_directory == loc.Msg.Not_added_abbr:
      self.working_directory_err.activate()
    else:
      result = tkinter.messagebox.askquestion(loc.SIMAGRI_MAIN_UI_Title, "Do you want to copy SIMAGRI module to working directory?",
                                        icon = 'question')
      if result == 'yes':
        srcdir = path.join(CurCWD, 'module')
        destdir = self._Setting.ScenariosSetup.Working_directory

        # make working directory after checking it
        if os.path.isdir(destdir) == False:
          rst_make_workdir = tkinter.messagebox.askquestion(loc.SIMAGRI_MAIN_UI_Title,
                                            'Do you want to make the working directory?',
                                            icon = 'question')
          if rst_make_workdir == 'yes':
            try:
              os.makedirs(destdir)
            except IOError as e:
              if e.errno == 13: # Permission denied
                # Can't make working directory due to permission denied
                # Please change other working directory
                self.make_working_directory_error.activate()
                return None

        # copy CAMDT-DiCAMDT to the destination directory
        try:
            SIMAGRIUtil.CopyModuleFiles(srcdir, str(destdir))
            print ("Copied SIMAGRI-DISEASE module to", destdir)
        except IOError as e:
            if e.errno == 13:  # Permission denied
                # Can't copy SIMAGRI modules to working directory due to permission denied
                # Please change other working directory
                self.copy_camdt_module_error.activate()

  # initialize UI (user interface) with Setting
  def initUI_Setting(self):
    self.WDir_label.configure(text = self._Setting.ScenariosSetup.Working_directory)
    self.WDir_label.configure(background = 'lavenderblush1')
    self._Setting.ScenariosSetup.WIfS_crop1.configure(text = loc.Msg.Not_added_abbr, background = 'SystemButtonFace')
    self._Setting.ScenariosSetup.WIfS_crop2.configure(text = loc.Msg.Not_added_abbr, background ='SystemButtonFace')
    self._Setting.ScenariosSetup.WIfS_crop3.configure(text = loc.Msg.Not_added_abbr, background ='SystemButtonFace')
    self._Setting.ScenariosSetup.WIfS_crop4.configure(text = loc.Msg.Not_added_abbr, background ='SystemButtonFace')
    self._Setting.ScenariosSetup.WIfS_crop5.configure(text = loc.Msg.Not_added_abbr, background ='SystemButtonFace')  
 
#=============================================
# check last observed data from *.WTD
def find_obs_date(WTD_fname):
    data1 = np.loadtxt(WTD_fname,skiprows=1)
    #convert numpy array to dataframe
    WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
        'DOY':data1[:,0].astype(int)%1000,
        'SRAD':data1[:,1],
        'TMAX':data1[:,2],
        'TMIN':data1[:,3],
        'RAIN':data1[:,4]})
    WTD_last_date = int(repr(WTD_df.YEAR.values[-1]) + repr(WTD_df.DOY.values[-1]).zfill(3))
    del WTD_df
    return WTD_last_date
#=============================================
