  # -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on November, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Original Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._compact import *

# ==========================Second page "DSSAT Setup 1"
class DSSATSetup1UI:

  __UI_Name__  = loc.DSSATSetup1.Title

  _UIParent = None
  _UINootbook = None

  _Setting = None

  def __init__(self, setting, parent, notebook):
    print("init %s Tab" % (self.__UI_Name__))

    self._UIParent = parent

    # set uivar variable for saving variables in simulation setup ui
    self._Setting = setting

    # =========================Third page for "DSSAT baseline setup - I "
    page3 = notebook.add(self.__UI_Name__)
    notebook.tab(self.__UI_Name__).focus_set()

    # 1) ADD SCROLLED FRAME
    sf_p1 = Pmw.ScrolledFrame(page3)  # ,usehullsize=1, hull_width = 700, hull_height=220)
    sf_p1.pack(padx = 5, pady = 3, fill = 'both', expand = YES)
    
      # set up "weather station"
    group11 = Pmw.Group(sf_p1.interior(), tag_text = 'Weather station',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
      #assign frame 1
    fm11=Frame(group11.interior())      
    Wstation_list = ("CNRA(Bambey)", "NRIP(Nioro)","SNTH(Sinthiou Malem)", "KOLD(Kolda)")
    self._Setting.DSSATSetup1.WStation = Pmw.ComboBox(fm11, label_text='Weather Station:', labelpos='wn',
                    listbox_width=20, dropdown=1,
                    selectioncommand = self.AvailableYears,
                    scrolledlist_items=Wstation_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.WStation.selectitem(Wstation_list[0])
    self._Setting.DSSATSetup1.WStation.pack(fill = 'x', side=LEFT,padx = 10, pady = 5)
    fm11.pack(fill='x', expand=1,padx=10,side=LEFT)

    fm12=Frame(group11.interior())         
    self.label11 = Label(fm12, text='Available data: From',padx=5, pady=5)
    self.label11.grid(row=0,column=0, sticky=W) #rowspan=1,columnspan=1)
    # self.label12 = Label(fm12, text='N/A',relief='sunken',padx=5, pady=5)
    # self.label12.grid(row=0,column=1, sticky=W) #rowspan=1,columnspan=1)
    self._Setting.DSSATSetup1.avail_year1 = Label(fm12, text='N/A',relief='sunken',padx=5, pady=5)
    self._Setting.DSSATSetup1.avail_year1.grid(row=0,column=1, sticky=W) #rowspan=1,columnspan=1)
    self.label13 = Label(fm12, text='To:',padx=5, pady=5)
    self.label13.grid(row=0,column=2, sticky=W) #rowspan=1,columnspan=1)
    # self.label14 = Label(fm12, text='N/A',relief='sunken',padx=5, pady=5)
    # self.label14.grid(row=0,column=3, sticky=W) #rowspan=1,columnspan=1)
    self._Setting.DSSATSetup1.avail_year2 = Label(fm12, text='N/A',relief='sunken',padx=5, pady=5)
    self._Setting.DSSATSetup1.avail_year2.grid(row=0,column=3, sticky=W) #rowspan=1,columnspan=1)
    fm12.pack(fill='x', expand=1,padx=10,side=LEFT)  

    # set up "simulation period"
    group12 = Pmw.Group(sf_p1.interior(), tag_text = 'Simulation Period',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.sim_year1 = Pmw.EntryField(group12.interior(), labelpos = 'w',
        label_text = 'Start Year:',
        validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2020, 'minstrict' : 0})
    self._Setting.DSSATSetup1.sim_year1.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    
    self._Setting.DSSATSetup1.sim_year2 = Pmw.EntryField(group12.interior(), labelpos = 'w',
        label_text = 'End Year:',
        validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2020, 'minstrict' : 0})
    self._Setting.DSSATSetup1.sim_year2.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)

    self._Setting.DSSATSetup1.sim_year1.setentry("1981")  #!!!!!!!!!!!!!!!!!!=TEMPORARY    
    self._Setting.DSSATSetup1.sim_year2.setentry("2015")  #!!!!!!!!!!!!!!!!!!=TEMPORARY   

    # select Crop type (Millet or Peanut?)
    group13 = Pmw.Group(sf_p1.interior(), tag_text = 'Crop to Plant',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group13.pack(fill='both', expand=1, side=TOP, padx = 10, pady = 5)    
  
      # Radio button to select "crop type"
    self._Setting.DSSATSetup1.Crop_type = tkinter.IntVar()  # Rbutton2
    Crop_option = [('Peanut', 0), ('Millet', 1), ('Sorghum', 2)]
    for text, value in Crop_option:
      Radiobutton(group13.interior(), text = text, command = self.empty_crop_selection_label,
          value = value, variable = self._Setting.DSSATSetup1.Crop_type).pack(side = LEFT, expand = YES)
    self._Setting.DSSATSetup1.Crop_type.set(1)   #By default- Millet

    # Create button to launch the dialog 
    crop_button=tkinter.Button(sf_p1.interior(),
            text = 'Click to add more details for the selected crop',
            command = self.getCropInput,bg='gray70').pack(side=TOP,anchor=N)

    # Copy DSSAT inpput from cultivar dialog
    frame_13 =Frame(sf_p1.interior())
    label_1 = Label(frame_13, text='Cultivar Type:', padx=5, pady=5)
    label_1.grid(row=0,column=0,sticky=E)
    # self.label_01 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=20) 
    # self.label_01.grid(row=0,column=1,sticky=W)
    self._Setting.DSSATSetup1.CUL_Cal_CulName = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=20) 
    self._Setting.DSSATSetup1.CUL_Cal_CulName.grid(row=0,column=1,sticky=W)
    label_2 = Label(frame_13, text='Soil Type:', padx=5, pady=5)
    label_2.grid(row=0,column=2,sticky=E)
    # self.label_02 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=15) 
    # self.label_02.grid(row=0,column=3,sticky=W)
    self._Setting.DSSATSetup1.Soil_Type = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=15) 
    self._Setting.DSSATSetup1.Soil_Type.grid(row=0,column=3,sticky=W)
    label_3 = Label(frame_13, text='Initial H2O:', padx=5, pady=5)
    label_3.grid(row=1,column=0,sticky=E)
    # self.label_03 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=20)  
    # self.label_03.grid(row=1,column=1,sticky=W)
    self._Setting.DSSATSetup1.IC_H2O = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=20) 
    self._Setting.DSSATSetup1.IC_H2O.grid(row=1,column=1,sticky=W)
    label_4 = Label(frame_13, text='Initial NO3:', padx=5, pady=5)
    label_4.grid(row=2,column=0,sticky=E)
    # self.label_04 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=20) 
    # self.label_04.grid(row=2,column=1,sticky=W)
    self._Setting.DSSATSetup1.IC_NO3 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=20)
    self._Setting.DSSATSetup1.IC_NO3.grid(row=2,column=1,sticky=W)
    label_8 = Label(frame_13, text='[N kg/ha] in top 30cm soil', padx=5, pady=5)
    label_8.grid(row=2,column=2,sticky=W)
    # label_5 = Label(frame_13, text='Planting Date(DOY):', padx=5, pady=5)
    # label_5.grid(row=2,column=2,sticky=E)
    # # self.label_05 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken') #,width=15)  
    # # self.label_05.grid(row=2,column=3,sticky=W)
    # self._Setting.DSSATSetup1.group14 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken') 
    # self._Setting.DSSATSetup1.group14.grid(row=2,column=3,sticky=W)
    label_6 = Label(frame_13, text='Planting Density:', padx=5, pady=5)
    label_6.grid(row=1,column=2,sticky=E)
    # self.label_06 = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=15)  
    # self.label_06.grid(row=1,column=3 ,sticky=W)
    self._Setting.DSSATSetup1.plt_density = Label(frame_13, text=loc.Msg.Not_added_abbr,relief='sunken',width=15)  
    self._Setting.DSSATSetup1.plt_density.grid(row=1,column=3 ,sticky=W)

    label_7 = Label(frame_13, text='[plants/m2]', padx=5, pady=5)
    label_7.grid(row=1,column=4,sticky=W)
    frame_13.pack(fill = 'both', expand = 1,side=TOP)   

    # set up "Planting date"
    group14 = Pmw.Group(sf_p1.interior(), tag_text = 'Planting Date',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group14.pack(fill = 'both', side=TOP, padx = 10, pady = 20) 
    self._Setting.DSSATSetup1.plt_month = Pmw.EntryField(group14.interior(), labelpos = 'w',
        label_text = 'Month:',
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 12, 'minstrict' : 1})
    self._Setting.DSSATSetup1.plt_month.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)

    self._Setting.DSSATSetup1.plt_date = Pmw.EntryField(group14.interior(), labelpos = 'w',
        label_text = 'Day:',
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 31, 'minstrict' : 1})
    self._Setting.DSSATSetup1.plt_date.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)

    # self._Setting.DSSATSetup1.plt_year.setentry("2010")  #!!!!!!!!!!!!!!!!!!=TEMPORARY    
    self._Setting.DSSATSetup1.plt_month.setentry("6")  #!!!!!!!!!!!!!!!!!!=TEMPORARY   
    self._Setting.DSSATSetup1.plt_date.setentry("15")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  

    # Init. Dialogs
    self.__initDialog()

  def __initDialog(self):
    #Dialog to get specific input for Peanut
    self.crop_dialog_PN = Pmw.Dialog(self._UIParent, title='Input for Peanut')
      # set up "soil information"
    group_c11 = Pmw.Group(self.crop_dialog_PN.interior(), tag_text = 'Soil',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    #soil types
    soil_list = ("CNCNioro14(S)", "CNCNNior15(SL)", "CNBambey14(LS)", #from Adama
                 "CNNior14_S(S)", "CNNior15_S(SL)", "CNBamb14_S(LS)", #from Adama-SRGF adjusted
                 "SN00840067(SL)", "SN00840080(SL)", "SN00840042(SL)", "SN00840056(SL)", "None")
    self._Setting.DSSATSetup1.PN_soil_type = Pmw.ComboBox(group_c11.interior(), label_text='Soil type:', labelpos='wn',
                    listbox_width=35, dropdown=1,
                    scrolledlist_items=soil_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.PN_soil_type.pack(fill = 'both',expand=1, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.PN_soil_type.selectitem(soil_list[2])

    # set up "cultivar selection"
    group_c12 = Pmw.Group(self.crop_dialog_PN.interior(), tag_text = 'Cultivar',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    cul_list = ("IB0090 VAR_FLEUR_11", "IB0091 VAR_73-33", "None") #"IB0034 897 VIRGINIA BUN",
    self._Setting.DSSATSetup1.PN_cul_type = Pmw.ComboBox(group_c12.interior(), label_text='Cultivar type:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=cul_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.PN_cul_type.pack(fill = 'x',side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.PN_cul_type.selectitem(cul_list[0]) 

    # set up "initial H2O condition"
    group_c13 = Pmw.Group(self.crop_dialog_PN.interior(), tag_text = 'Initial soil H20',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c13.pack(fill = 'x', side=TOP, padx = 10, pady = 2)   
    # wetness_list = ("0.3(30% of AWC)","0.5(50% of AWC)","0.7(70% of AWC)","1.0(100% of AWC)","None")
    wetness_list = ("0.1(10% of AWC)","0.2(20% of AWC)","0.3(30% of AWC)","0.4(40% of AWC)","0.5(50% of AWC)",
                     "0.6(60% of AWC)","0.7(70% of AWC)","0.8(80% of AWC)","0.9(90% of AWC)","1.0(100% of AWC)","None")
    self._Setting.DSSATSetup1.PN_ini_H2O = Pmw.ComboBox(group_c13.interior(), label_text='Wetness:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=wetness_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.PN_ini_H2O.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.PN_ini_H2O.selectitem(wetness_list[2])

    # set up "initial NO3 condition"
    group_c14 = Pmw.Group(self.crop_dialog_PN.interior(), tag_text = 'Initial soil NO3 in top 30cm soil',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c14.pack(fill = 'x', side=TOP, padx = 10, pady = 2)  
    # NO3_list = ("High(65 N kg/ha)","Low(23 N kg/ha)")
    # self._Setting.DSSATSetup1.PN_ini_NO3 = Pmw.ComboBox(group_c14.interior(), label_text='Nitrate level:', labelpos='wn',
    #                 listbox_width=15, dropdown=1,
    #                 scrolledlist_items=NO3_list,
    #                 entryfield_entry_state=DISABLED)
    # self._Setting.DSSATSetup1.PN_ini_NO3.pack(fill = 'x',side=LEFT, padx = 10, pady = 2)
    # self._Setting.DSSATSetup1.PN_ini_NO3.selectitem(NO3_list[1]) 
    self._Setting.DSSATSetup1.PN_ini_NO3 = Pmw.EntryField(group_c14.interior(), labelpos = 'w', label_text = 'Nitrate content [N kg/ha]:', validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.PN_ini_NO3.pack(fill = 'x', side=TOP, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.PN_ini_NO3.setentry("20")  #!!!!!!!!!!!!!!!!!!=TEMPORARY
    frame_peanut =Frame(group_c14.interior())
    label_peanut = Label(frame_peanut, text=' *[Reference] Low Nitrate:20 N kg/ha (~ 4.8 ppm)', padx=1, pady=1)
    # label_peanut.place(relx = 0.0, rely = 1.0, anchor ='sw')
    label_peanut.grid(row=0,column=0,sticky=W)
    label_peanut2 = Label(frame_peanut, text='                   High Nitrate: 85 N kg/ha (~20 ppm)', padx=1, pady=1)
    label_peanut2.grid(row=1,column=0,sticky=W)
    frame_peanut.pack(fill = 'x', expand = 1,side=TOP)  
    # set up "Planting density"
    group_c15 = Pmw.Group(self.crop_dialog_PN.interior(),tag_text = 'Planting Density [plants/m2]',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_c15.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.PN_plt_density = Pmw.EntryField(group_c15.interior(), labelpos = 'w', label_text = 'Planting Density:', validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.PN_plt_density.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.PN_plt_density.setentry("3")  #!!!!!!!!!!!!!!!!!!=TEMPORARY
     
    self.crop_dialog_PN.withdraw()
    #========end of crop_dialog for Peanut
    
    #Dialog to get specific input for Millet
    self.crop_dialog_ML = Pmw.Dialog(self._UIParent, title='Input for Millet')
      # set up "soil information"
    group_b11 = Pmw.Group(self.crop_dialog_ML.interior(), tag_text = 'Soil',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    #soil types
    soil_list2 = ("CNCNioro14(S)", "CNCNNior15(SL)", "CNBambey14(LS)", #from Adama
                 "CNNior14_S(S)", "CNNior15_S(SL)", "CNBamb14_S(LS)", #from Adama-SRGF adjusted
                 "SN00840067(SL)", "SN00840080(SL)", "SN00840042(SL)", "SN00840056(SL)", "None")
    self._Setting.DSSATSetup1.ML_soil_type = Pmw.ComboBox(group_b11.interior(), label_text='Soil type:', labelpos='wn',
                    listbox_width=35, dropdown=1,
                    scrolledlist_items=soil_list2,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.ML_soil_type.pack(fill = 'both',expand=1, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.ML_soil_type.selectitem(soil_list2[0])
    
    # set up "cultivar selection"
    group_b12 = Pmw.Group(self.crop_dialog_ML.interior(), tag_text = 'Cultivar',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    cul_list2 = ("IB0044 CIVT","None")
    self._Setting.DSSATSetup1.ML_cul_type = Pmw.ComboBox(group_b12.interior(), label_text='Cultivar type:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=cul_list2,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.ML_cul_type.pack(fill = 'x',side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.ML_cul_type.selectitem(cul_list2[0])

    # set up "initial H2O condition"
    group_b13 = Pmw.Group(self.crop_dialog_ML.interior(), tag_text = 'Initial soil H20',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b13.pack(fill = 'x', side=TOP, padx = 10, pady = 2)   
    # wetness_list = ("0.3(30% of AWC)","0.5(50% of AWC)","0.7(70% of AWC)","1.0(100% of AWC)","None")
    wetness_list = ("0.1(10% of AWC)","0.2(20% of AWC)","0.3(30% of AWC)","0.4(40% of AWC)","0.5(50% of AWC)",
                     "0.6(60% of AWC)","0.7(70% of AWC)","0.8(80% of AWC)","0.9(90% of AWC)","1.0(100% of AWC)","None")
    self._Setting.DSSATSetup1.ML_ini_H2O = Pmw.ComboBox(group_b13.interior(), label_text='Wetness:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=wetness_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.ML_ini_H2O.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.ML_ini_H2O.selectitem(wetness_list[2])

    # set up "initial NO3 condition"
    group_b14 = Pmw.Group(self.crop_dialog_ML.interior(), tag_text = 'Initial soil NO3',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b14.pack(fill = 'x', side=TOP, padx = 10, pady = 2)  
    # NO3_list2 = ("High(65 N kg/ha)","Low(23 N kg/ha)")
    # self._Setting.DSSATSetup1.ML_ini_NO3 = Pmw.ComboBox(group_b14.interior(), label_text='Nitrate level:', labelpos='wn',
    #                 listbox_width=15, dropdown=1,
    #                 scrolledlist_items=NO3_list2,
    #                 entryfield_entry_state=DISABLED)
    # self._Setting.DSSATSetup1.ML_ini_NO3.pack(fill = 'x',side=LEFT, padx = 10, pady = 2)
    # self._Setting.DSSATSetup1.ML_ini_NO3.selectitem(NO3_list2[1])  
    self._Setting.DSSATSetup1.ML_ini_NO3 = Pmw.EntryField(group_b14.interior(), labelpos = 'w', label_text = 'Nitrate content [N kg/ha]:', validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.ML_ini_NO3.pack(fill = 'x', side=TOP, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.ML_ini_NO3.setentry("20")  #!!!!!!!!!!!!!!!!!!=TEMPORARY
    frame_ML =Frame(group_b14.interior())
    label_ML = Label(frame_ML, text=' *[Reference] Low Nitrate:20 N kg/ha (~ 4.8 ppm)', padx=1, pady=1)
    label_ML.grid(row=0,column=0,sticky=W)
    label_ML2 = Label(frame_ML, text='                   High Nitrate: 85 N kg/ha (~20 ppm)', padx=1, pady=1)
    label_ML2.grid(row=1,column=0,sticky=W)
    frame_ML.pack(fill = 'x', expand = 1,side=TOP)    
    # set up "Planting density"
    group_b15 = Pmw.Group(self.crop_dialog_ML.interior(),tag_text = 'Planting Density [plants/m2]',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_b15.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.ML_plt_density = Pmw.EntryField(group_b15.interior(), labelpos = 'w', label_text = 'Planting Density:',validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.ML_plt_density.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.ML_plt_density.setentry("3")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  
    self.crop_dialog_ML.withdraw()
    #========end of crop_dialog for Millet

    #Dialog to get specific input for Sorghum
    self.crop_dialog_SG = Pmw.Dialog(self._UIParent, title='Input for Sorghum')
      # set up "soil information"
    group_s11 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Soil',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    #soil types
    soil_list3 = ("SN-N15Rain(S)", "SN-N15Irrg(S)", "SN-N16Rain(S)", "SN-N16Irrg(S)", "SN-S15Rain(LS)","SN-S16Rain(LS)",#from Ganyo(2019) sorghum
                 "SN00840067(SL)", "SN00840080(SL)", "SN00840042(SL)", "SN00840056(SL)", "None")
    self._Setting.DSSATSetup1.SG_soil_type = Pmw.ComboBox(group_s11.interior(), label_text='Soil type:', labelpos='wn',
                    listbox_width=35, dropdown=1,
                    scrolledlist_items=soil_list3,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.SG_soil_type.pack(fill = 'both',expand=1, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.SG_soil_type.selectitem(soil_list3[0])
    
    # set up "cultivar selection"
    group_s12 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Cultivar',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    cul_list3 = ("IB0066 Fadda-D","IB0069 IS15401-D","IB0070 Soumba-D","IB0071 Faourou-D","None")
    self._Setting.DSSATSetup1.SG_cul_type = Pmw.ComboBox(group_s12.interior(), label_text='Cultivar type:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=cul_list3,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.SG_cul_type.pack(fill = 'x',side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.SG_cul_type.selectitem(cul_list3[0])

    # set up "initial H2O condition"
    group_s13 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Initial soil H20',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s13.pack(fill = 'x', side=TOP, padx = 10, pady = 2)   
    # wetness_list = ("0.3(30% of AWC)","0.5(50% of AWC)","0.7(70% of AWC)","1.0(100% of AWC)","None")
    wetness_list = ("0.1(10% of AWC)","0.2(20% of AWC)","0.3(30% of AWC)","0.4(40% of AWC)","0.5(50% of AWC)",
                     "0.6(60% of AWC)","0.7(70% of AWC)","0.8(80% of AWC)","0.9(90% of AWC)","1.0(100% of AWC)","None")
    self._Setting.DSSATSetup1.SG_ini_H2O = Pmw.ComboBox(group_s13.interior(), label_text='Wetness:', labelpos='wn',
                    listbox_width=15, dropdown=1,
                    scrolledlist_items=wetness_list,
                    entryfield_entry_state=DISABLED)
    self._Setting.DSSATSetup1.SG_ini_H2O.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
    self._Setting.DSSATSetup1.SG_ini_H2O.selectitem(wetness_list[2])

    #set up "initial NO3 condition"
    group_s14 = Pmw.Group(self.crop_dialog_SG.interior(), tag_text = 'Initial soil NO3',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s14.pack(fill = 'x', side=TOP, padx = 10, pady = 2)  
    # NO3_list2 = ("High(65 N kg/ha)","Low(23 N kg/ha)")
    # self._Setting.DSSATSetup1.SG_ini_NO3 = Pmw.ComboBox(group_s14.interior(), label_text='Nitrate level:', labelpos='wn',
    #                 listbox_width=15, dropdown=1,
    #                 scrolledlist_items=NO3_list2,
    #                 entryfield_entry_state=DISABLED)
    # self._Setting.DSSATSetup1.SG_ini_NO3.pack(fill = 'x',side=LEFT, padx = 10, pady = 2)
    # self._Setting.DSSATSetup1.SG_ini_NO3.selectitem(NO3_list2[1])
    self._Setting.DSSATSetup1.SG_ini_NO3 = Pmw.EntryField(group_s14.interior(), labelpos = 'w', label_text = 'Nitrate content [N kg/ha]:', validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.SG_ini_NO3.pack(fill = 'x', side=TOP, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.SG_ini_NO3.setentry("20")  #!!!!!!!!!!!!!!!!!!=TEMPORARY
    frame_SG =Frame(group_s14.interior())
    label_SG = Label(frame_SG, text=' *[Reference] Low Nitrate:20 N kg/ha (~ 4.8 ppm)', padx=1, pady=1)
    # label_SG.place(relx = 0.0, rely = 1.0, anchor ='sw')
    label_SG.grid(row=0,column=0,sticky=W)
    label_SG2 = Label(frame_SG, text='                   High Nitrate: 85 N kg/ha (~20 ppm)', padx=1, pady=1)
    label_SG2.grid(row=1,column=0,sticky=W)
    frame_SG.pack(fill = 'x', expand = 1,side=TOP) 
    # set up "Planting density"
    group_s15 = Pmw.Group(self.crop_dialog_SG.interior(),tag_text = 'Planting Density [plants/m2]',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    group_s15.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
    self._Setting.DSSATSetup1.SG_plt_density = Pmw.EntryField(group_s15.interior(), labelpos = 'w', label_text = 'Planting Density:', validate = {'validator': 'real'})
    self._Setting.DSSATSetup1.SG_plt_density.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
    self._Setting.DSSATSetup1.SG_plt_density.setentry("6.25")  #!!!!!!!!!!!!!!!!!!=TEMPORARY 
    self.crop_dialog_SG.withdraw()
    #========end of crop_dialog for sorghum

  #Get input for each cultivar (planting date, soil, IC etc)
  def getCropInput(self):
    if self._Setting.DSSATSetup1.Crop_type.get() == 0: #Peanut
      self.crop_dialog_PN.activate()
      if self._Setting.DSSATSetup1.PN_cul_type.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.CUL_Cal_CulName.configure(text=self._Setting.DSSATSetup1.PN_cul_type.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in cultivar select',
                    defaultbutton = 0,
                    message_text = 'No cultivar type is chosen!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
      if self._Setting.DSSATSetup1.PN_soil_type.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.Soil_Type.configure(text=self._Setting.DSSATSetup1.PN_soil_type.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in soil select',
                    defaultbutton = 0,
                    message_text = 'No soil type is chosen!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
      if self._Setting.DSSATSetup1.PN_ini_H2O.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.IC_H2O.configure(text=self._Setting.DSSATSetup1.PN_ini_H2O.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in initial H2O',
                    defaultbutton = 0,
                    message_text = 'No initial soil H2O condition is selected!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
      self._Setting.DSSATSetup1.IC_NO3.configure(text=self._Setting.DSSATSetup1.PN_ini_NO3.getvalue(),background='honeydew1')
      # self.label_04.configure(text='Low(5 ppm NO3)',background='honeydew1')
      self._Setting.DSSATSetup1.plt_density.configure(text=self._Setting.DSSATSetup1.PN_plt_density.getvalue(),background='honeydew1')

    #===Millet
    elif self._Setting.DSSATSetup1.Crop_type.get() == 1: #Millet
      self.crop_dialog_ML.activate()
      if self._Setting.DSSATSetup1.ML_cul_type.getvalue()[0][0:4]  != "None":
        # self.label_01.configure(text=self._Setting.DSSATSetup1.ML_cul_type.getvalue()[0],background='honeydew1')
        self._Setting.DSSATSetup1.CUL_Cal_CulName.configure(text=self._Setting.DSSATSetup1.ML_cul_type.getvalue()[0],background='honeydew1') 
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in cultivar select',
                    defaultbutton = 0,
                    message_text = 'No cultivar type is chosen!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
        print('error message is ', result)
      if self._Setting.DSSATSetup1.ML_soil_type.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.Soil_Type.configure(text = self._Setting.DSSATSetup1.ML_soil_type.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in soil select',
                    defaultbutton = 0,
                    message_text = 'No soil type is chosen!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
      if self._Setting.DSSATSetup1.ML_ini_H2O.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.IC_H2O.configure(text=self._Setting.DSSATSetup1.ML_ini_H2O.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in initial H2O',
                    defaultbutton = 0,
                    message_text = 'No initial soil H2O condition is selected!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
      self._Setting.DSSATSetup1.IC_NO3.configure(text=self._Setting.DSSATSetup1.ML_ini_NO3.getvalue(),background='honeydew1')
      self._Setting.DSSATSetup1.plt_density.configure(text=self._Setting.DSSATSetup1.ML_plt_density.getvalue(),background='honeydew1')
      # # self._Setting.DSSATSetup1.group14.configure(text=self._Setting.DSSATSetup1.ML_group14.getvalue(),background='honeydew1')
      # if self._Setting.DSSATSetup1.ML_group14.getvalue() != "":
      #   self._Setting.DSSATSetup1.group14.configure(text=self._Setting.DSSATSetup1.ML_group14.getvalue(),background='honeydew1')
      # else: 
      #   dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in planting date',
      #               defaultbutton = 0,
      #               message_text = 'No planting date is typed!')
      #   dialog.iconname('error message dialog')
      #   result=dialog.activate()
    #===sorghum
    else:  #Sorguhm
      self.crop_dialog_SG.activate()
      if self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.CUL_Cal_CulName.configure(text=self._Setting.DSSATSetup1.SG_cul_type.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in cultivar select',
                    defaultbutton = 0,
                    message_text = 'No cultivar type is chosen!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
        print('error message is ', result)
      if self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.Soil_Type.configure(text = self._Setting.DSSATSetup1.SG_soil_type.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in soil select',
                    defaultbutton = 0,
                    message_text = 'No soil type is chosen!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
      if self._Setting.DSSATSetup1.SG_ini_H2O.getvalue()[0][0:4]  != "None":
        self._Setting.DSSATSetup1.IC_H2O.configure(text=self._Setting.DSSATSetup1.SG_ini_H2O.getvalue()[0],background='honeydew1')
      else: 
        dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in initial H2O',
                    defaultbutton = 0,
                    message_text = 'No initial soil H2O condition is selected!')
        dialog.iconname('error message dialog')
        result=dialog.activate()
      self._Setting.DSSATSetup1.IC_NO3.configure(text=self._Setting.DSSATSetup1.SG_ini_NO3.getvalue(),background='honeydew1')
      self._Setting.DSSATSetup1.plt_density.configure(text=self._Setting.DSSATSetup1.SG_plt_density.getvalue(),background='honeydew1')
      # # self._Setting.DSSATSetup1.group14.configure(text=self._Setting.DSSATSetup1.SG_group14.getvalue(),background='honeydew1')
      # if self._Setting.DSSATSetup1.SG_group14.getvalue() != "":
      #   self._Setting.DSSATSetup1.group14.configure(text=self._Setting.DSSATSetup1.SG_group14.getvalue(),background='honeydew1')
      # else: 
      #   dialog = Pmw.MessageDialog(self._UIParent, title = 'Error message in planting date',
      #               defaultbutton = 0,
      #               message_text = 'No planting date is typed!')
      #   dialog.iconname('error message dialog')
      #   result=dialog.activate()          

  def AvailableYears(self, string):
    if string[0:4] == 'NIOR' or string[0:4] == 'SINT' or string[0:4] == 'ZIGU' or string[0:4] == 'BAMB':
      s_year='1981'
      e_year='2015'
    elif string[0:4] == 'CNRA':
      s_year='1950'
      e_year='2015'
    elif string[0:4] == 'NRIP':
      s_year='1950'
      e_year='2016'
    else:   ##SNTH and KOLD
      s_year='1981'
      e_year='2015'
    self._Setting.DSSATSetup1.avail_year1.configure(text=s_year,background='honeydew1')
    self._Setting.DSSATSetup1.avail_year2.configure(text=e_year,background='honeydew1')

  def empty_crop_selection_label(self):
    self._Setting.DSSATSetup1.CUL_Cal_CulName.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self._Setting.DSSATSetup1.Soil_Type.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self._Setting.DSSATSetup1.IC_H2O.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self._Setting.DSSATSetup1.IC_NO3.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self._Setting.DSSATSetup1.plt_density.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')


  # initialize UI (user interface) with Setting
  def initUI_Setting(self):
    # self.empty_cultivar_selection_label()
    self.empty_crop_selection_label()

    # if self._Setting.DSSATSetup1.Crop_type.get() == 1:  #Millet
    #   self.label_01.configure(text=self._Setting.DSSATSetup1.ML_cul_type.getvalue()[0],background='honeydew1')
    #   self.label_02.configure(text=self._Setting.DSSATSetup1.ML_soil_type.getvalue()[0],background='honeydew1')
    #   self.label_03.configure(text=self._Setting.DSSATSetup1.ML_ini_H2O.getvalue()[0],background='honeydew1')
    #   self.label_04.configure(text=self._Setting.DSSATSetup1.ML_ini_NO3.getvalue()[0],background='honeydew1')
    # else:  #dry bean
    #   self.label_01.configure(text=self._Setting.DSSATSetup1.BN_cul_type.getvalue()[0],background='honeydew1')
    #   self.label_02.configure(text = self._Setting.DSSATSetup1.BN_soil_type.getvalue()[0],background='honeydew1')
    #   self.label_03.configure(text=self._Setting.DSSATSetup1.BN_ini_H2O.getvalue()[0],background='honeydew1')
    #   self.label_04.configure(text='Low(5 ppm NO3)',background='honeydew1')
