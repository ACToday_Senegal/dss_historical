# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on October, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._compact import *


# ==========================Second page "DSSAT Setup 2"
class DSSATSetup2UI:
  __UI_Name__ = loc.DSSATSetup2.Title

  _UIParent = None  ## _single_leading_underscore => private variable
  _UINootbook = None  ## private variable

  _Setting = None

  def __init__(self, setting, parent, notebook):
    print ("init %s Tab" % (self.__UI_Name__))

    self._UIParent = parent

    # set uivar variable for saving variables in simulation setup ui
    self._Setting = setting

    # =========================4th page for "DSSAT baseline setup - II "
    page4 = notebook.add('DSSAT setup 2')
    notebook.tab('DSSAT setup 2').focus_set()
    # 1) ADD SCROLLED FRAME
    sf_p4 = Pmw.ScrolledFrame(page4)  # ,usehullsize=1, hull_width=700, hull_height=220)
    sf_p4.pack(padx = 5, pady = 3, fill = 'both', expand = YES)

    # assign frame 1
    fm41 = Frame(sf_p4.interior())
    # Radio button to select "Irrigation method"
    group41 = Pmw.Group(fm41, tag_text = 'Fertilization application', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group41.pack(fill = 'both', expand = 1, side = TOP, padx = 10, pady = 5)

    self._Setting.DSSATSetup2.rbFertApp = tkinter.IntVar()  # self.Fbutton1
    Frt_option = [('Fertilization', 0), ('No Fertilization', 1)]
    for text, value in Frt_option:
      Radiobutton(group41.interior(), text = text, command = self.empty_fert_label,
                  value = value, variable = self._Setting.DSSATSetup2.rbFertApp).pack(side = LEFT, expand = YES)
    self._Setting.DSSATSetup2.rbFertApp.set(1)  # By default- no Fertilization
    # Create button to launch the dialog
    frt_button = tkinter.Button(fm41,
                                text = 'Click to add more details for fertilizer',
                                command=self.getFertInput, bg='gray70').pack(side = TOP, anchor = N)

    # Create the "fertilizer" contents of the page.
    group42 = Pmw.Group(fm41, tag_text = 'Fertilizer application', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group42.pack(fill = 'x', side = TOP, expand = 1, padx = 2, pady = 5)
    # self._Setting.DSSATSetup2.FA_nfertilizer = Pmw.EntryField(group42.interior(),  # self.nfertilizer
    #                                   labelpos = 'w', label_text = 'Number of fertilizer applications? ',
    #                                   validate = {'validator': 'numeric', 'min': 1, 'max': 3, 'minstrict': 0})
    # self._Setting.DSSATSetup2.FA_nfertilizer.pack(side = TOP, anchor = W, padx = 5, pady = 5)
    frame_frt = Frame(group42.interior())
    label000 = Label(frame_frt, text = 'No. application', padx = 5, pady = 5)
    label001 = Label(frame_frt, text = 'Days after planting', padx = 5, pady = 5)
    # label002 = Label(frame_frt, text = 'Material', padx = 5, pady = 5)
    # label003 = Label(frame_frt, text = 'Application method', padx = 5, pady = 5)
    label004 = Label(frame_frt, text = 'Depth (cm)', padx = 5, pady = 5)
    label005 = Label(frame_frt, text = 'N (kg/ha)', padx = 5, pady = 5)
    label006 = Label(frame_frt, text = 'P (kg/ha)', padx = 5, pady = 5)
    label007 = Label(frame_frt, text = 'K (kg/ha)', padx = 5, pady = 5)
    # self.label005 = Label(frame_frt, text = '1st:', padx = 5, pady = 5)
    self.label1_1 = Label(frame_frt, text = '1st:', padx = 5, pady = 5)
    self.label1_2 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    # self.label1_3 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)
    # self.label1_4 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=25)
    self.label1_5 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label1_6 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label1_7 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label1_8 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    # self.label010 = Label(frame_frt, text = '2nd:', padx = 5, pady = 5)
    self.label2_1 = Label(frame_frt, text = '2nd:', padx = 5, pady = 5)
    self.label2_2 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    # self.label2_3 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)
    # self.label2_4 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=25)
    self.label2_5 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label2_6 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label2_7 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label2_8 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    # self.label015 = Label(frame_frt, text = '3rd:', padx = 5, pady = 5)
    self.label3_1 = Label(frame_frt, text = '3rd:', padx = 5, pady = 5)
    self.label3_2 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    # self.label3_3 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)
    # self.label3_4 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=25)
    self.label3_5 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label3_6 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label3_7 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label3_8 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label4_1 = Label(frame_frt, text = '4th:', padx = 5, pady = 5)
    self.label4_2 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    # self.label4_3 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)
    # self.label4_4 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=25)
    self.label4_5 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label4_6 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label4_7 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label4_8 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label5_1 = Label(frame_frt, text = '5th:', padx = 5, pady = 5)
    self.label5_2 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    # self.label5_3 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)
    # self.label5_4 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=25)
    self.label5_5 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label5_6 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label5_7 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    self.label5_8 = Label(frame_frt, text = loc.Msg.Not_added_abbr, relief='sunken', width=9)
    # aline  empty label
    entries = (label000, label001, label004, label005, label006, label007)
    i = 0
    for entry in entries:
      entry.grid(row=0, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label1_1, self.label1_2, self.label1_5,self.label1_6,self.label1_7,self.label1_8)
    i = 0
    for entry in entries:
      entry.grid(row=1, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label2_1, self.label2_2, self.label2_5,self.label2_6,self.label2_7,self.label2_8)
    i = 0
    for entry in entries:
      entry.grid(row=2, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label3_1, self.label3_2, self.label3_5,self.label3_6,self.label3_7,self.label3_8)
    i = 0
    for entry in entries:
      entry.grid(row=3, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label4_1, self.label4_2, self.label4_5,self.label4_6,self.label4_7,self.label4_8)
    i = 0
    for entry in entries:
      entry.grid(row=4, column=i)  # , sticky=W)
      i = i + 1
    entries = (self.label5_1, self.label5_2, self.label5_5,self.label5_6,self.label5_7,self.label5_8)
    i = 0
    for entry in entries:
      entry.grid(row=5, column=i)  # , sticky=W)
      i = i + 1
    frame_frt.pack(fill = 'x', expand = 1, side = TOP)
    fm41.pack(fill = 'both', expand = 1, side = TOP)
    # assign frame 2
    fm42 = Frame(sf_p4.interior())
    # Radio button to select "Planting method"
    group43 = Pmw.Group(fm42, tag_text = 'Irrigation', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group43.pack(fill = 'x', expand = 1, side = TOP, padx = 10, pady = 5)

    self._Setting.DSSATSetup2.rbIrrigation = tkinter.IntVar()  # self.IRbutton
    IR_option = [('No Irrigation', 1),('Automatic when required', 0), ('On Reported Dates', 2)]
    for text, value in IR_option:
      Radiobutton(group43.interior(), text = text, command = self.empty_irrig_label,
                 value = value, variable = self._Setting.DSSATSetup2.rbIrrigation).pack(side = LEFT, expand = YES)
    self._Setting.DSSATSetup2.rbIrrigation.set(1)  # By default- no irrigation

    # Create button to launch the dialog
    Irr_button = tkinter.Button(fm42,
                                text = 'Click to add more details for irrigation',
                                command=self.getIrrInput, bg='gray70').pack(side = TOP, anchor = N)

    # Create the "Automatic irrigation" contents of the page.
    group44 = Pmw.Group(fm42, tag_text = 'Automatic Irrigation', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group44.pack(fill = 'x', side = TOP, expand = 1, padx = 2, pady = 5)
    self.label401 = Label(group44.interior(), text = 'Management depth(cm):', anchor = W, padx = 10)
    self.label401.grid(row=0, column=0, sticky=W)
    self.label402 = Label(group44.interior(), text = loc.Msg.Not_added_abbr, relief='sunken', padx = 5)
    self.label402.grid(row=0, column=1, sticky=W)
    self.label403 = Label(group44.interior(), text = 'Threshold(% of max available):', anchor = W, padx = 10)
    self.label403.grid(row=0, column=2, sticky=W)
    self.label404 = Label(group44.interior(), text = loc.Msg.Not_added_abbr, relief='sunken', padx = 5)
    self.label404.grid(row=0, column=3, sticky=W)
    ##        self.label405 = Label(group44.interior(), text = 'End point(% of max available):',anchor = W, padx = 10)
    ##        self.label405.grid(row=1,column=0, sticky=W)
    ##        self.label406 = Label(group44.interior(), text = loc.Msg.Not_added_abbr,relief='sunken',padx = 5)
    ##        self.label406.grid(row=1,column=1, sticky=W)
    self.label407 = Label(group44.interior(), text = 'Efficiency fraction:', anchor = W, padx = 10)
    self.label407.grid(row=0, column=4, sticky=W)
    self.label408 = Label(group44.interior(), text = loc.Msg.Not_added_abbr, relief='sunken', padx = 5)
    self.label408.grid(row=0, column=5, sticky=W)

    # Create the "manual irrigation" contents of the page.
    group45 = Pmw.Group(fm42, tag_text = 'Irrigation on Reported Dates', tag_font=Pmw.logicalfont('Helvetica', 0.5))
    group45.pack(fill = 'x', side = TOP, expand = 1, padx = 2, pady = 5)
    # self._Setting.DSSATSetup2.IR_nirrigation = Pmw.EntryField(group45.interior(),  # self.nirrigation
    #                                   labelpos = 'w', label_text = 'Number of irrigations? ',
    #                                   validate = {'validator': 'numeric', 'min': 1, 'max': 3, 'minstrict': 0})
    # self._Setting.DSSATSetup2.IR_nirrigation.pack(side = TOP, padx = 5, anchor = W, pady = 5)
    frame_in1 = Frame(group45.interior())
    label120 = Label(frame_in1, text = 'Method:', padx = 5)
    self.label121 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    label101 = Label(frame_in1, text = 'Date(MM/dd/yyyy)', width=18, padx = 5, pady = 5)
    label102 = Label(frame_in1, text = 'Amount of Water (mm)', width=18, padx = 5, pady = 5)
    self.label105 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label106 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)

    self.label109 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label110 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)

    self.label113 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label114 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)

    self.label115 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label116 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)

    self.label117 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=13)
    self.label118 = Label(frame_in1, text = loc.Msg.Not_added_abbr, relief='sunken', width=18)
    # aline  empty label
    entries = (label120, label101, self.label105, self.label109,self.label113,self.label115,self.label117)
    i = 0
    for entry in entries:
      entry.grid(row=i, column=0)  # , sticky=W)
      i = i + 1
    entries = (self.label121, label102, self.label106, self.label110, self.label114,self.label116,self.label118)
    i = 0
    for entry in entries:
      entry.grid(row=i, column=1)  # , sticky=W)
      i = i + 1
    frame_in1.pack(fill = 'x', expand = 1, side = TOP)
    fm42.pack(fill = 'x', expand = 1, side = TOP)

    # init. Dialogs
    self.__initDialogs()


  def __initDialogs(self):
    # ============to add more specification for fertilization information
    self.fert_dialog = Pmw.Dialog(self._UIParent, title = 'Fertilization Application')
    #========= 1st fertilizer application
    self.frt_group1 = Pmw.Group(self.fert_dialog.interior(), tag_text = '1st application')
    self.frt_group1.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day1 = Pmw.EntryField(self.frt_group1.interior(),  # self.day1
                               labelpos = 'n', label_text = 'Days after plt',
                               validate = {'validator': 'numeric', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # # -fertilizer material
    # material_list = ("FE001(Ammonium nitrate)","FE004(Anhydrous ammonia)","FE005(Urea)","FE014(Triple super phosphate)",
    #                 "FE006(Diammnoium phosphate)","FE017(Potassium nitrate)","FE0010(Urea ammonium nitrate solution)","None")
    # self._Setting.DSSATSetup2.FA_fert_mat1 = Pmw.ComboBox(self.frt_group1.interior(),  # fert_mat1
    #                                  label_text = 'Material', labelpos = 'n',
    #                                  listbox_width = 15, dropdown = 1,
    #                                  scrolledlist_items = material_list,
    #                                  entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_mat1.selectitem(material_list[7])
    # # -fertilizer application method
    # apply_list = ("AP001(Broadcast, not incorporated)","AP002(Broadcast, incorporated)",
    #               "AP003(Banded on surface)","AP004(Banded beneath surface)",
    #               "AP011(Broadcast on flooded/saturated soil, none in soil)","None")
    # self._Setting.DSSATSetup2.FA_fert_app_method1 = Pmw.ComboBox(self.frt_group1.interior(),  # fert_app1
    #                                                              label_text = 'Method', labelpos = 'n',
    #                                                              listbox_width = 15, dropdown = 1,
    #                                                              scrolledlist_items = apply_list,
    #                                                              entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_app_method1.selectitem(apply_list[5])
    self._Setting.DSSATSetup2.FA_amt_depth1 = Pmw.EntryField(self.frt_group1.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'Depth(cm)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 50, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_N1 = Pmw.EntryField(self.frt_group1.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'N (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_P1 = Pmw.EntryField(self.frt_group1.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'P (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_K1 = Pmw.EntryField(self.frt_group1.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'K (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day1, self._Setting.DSSATSetup2.FA_amt_depth1,
               self._Setting.DSSATSetup2.FA_amt_N1, self._Setting.DSSATSetup2.FA_amt_P1,self._Setting.DSSATSetup2.FA_amt_K1)
    for entry in entries:
      entry.pack(fill = 'x', side = LEFT, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)
    self._Setting.DSSATSetup2.FA_amt_depth1.setentry("2")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  2cm depth
    #========= 2nd fertilizer application
    self.frt_group2 = Pmw.Group(self.fert_dialog.interior(), tag_text = '2nd application')
    self.frt_group2.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day2 = Pmw.EntryField(self.frt_group2.interior(),   # day2
                                  labelpos = 'n', label_text = 'Days after plt',
                                  validate = {'validator': 'numeric', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # # -fertilizer material
    # self._Setting.DSSATSetup2.FA_fert_mat2 = Pmw.ComboBox(self.frt_group2.interior(),  # fert_mat2
    #                                  label_text = 'Material', labelpos = 'n',
    #                                  listbox_width = 15, dropdown = 1,
    #                                  scrolledlist_items = material_list,
    #                                  entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_mat2.selectitem(material_list[7])
    # # -fertilizer application method
    # self._Setting.DSSATSetup2.FA_fert_app_method2 = Pmw.ComboBox(self.frt_group2.interior(),  # fert_app2
    #                                         label_text = 'Method', labelpos = 'n',
    #                                         listbox_width = 15, dropdown = 1,
    #                                         scrolledlist_items = apply_list,
    #                                         entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_app_method2.selectitem(apply_list[5])
    self._Setting.DSSATSetup2.FA_amt_depth2 = Pmw.EntryField(self.frt_group2.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'Depth(cm)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 50, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_N2 = Pmw.EntryField(self.frt_group2.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'N (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_P2 = Pmw.EntryField(self.frt_group2.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'P (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_K2 = Pmw.EntryField(self.frt_group2.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'K (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day2, self._Setting.DSSATSetup2.FA_amt_depth2,
               self._Setting.DSSATSetup2.FA_amt_N2, self._Setting.DSSATSetup2.FA_amt_P2,self._Setting.DSSATSetup2.FA_amt_K2)
    for entry in entries:
      entry.pack(fill = 'x', side = LEFT, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)
    self._Setting.DSSATSetup2.FA_amt_depth2.setentry("2")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  2cm depth

    #=========== 3rd fertilizer application
    self.frt_group3 = Pmw.Group(self.fert_dialog.interior(), tag_text = '3rd application')
    self.frt_group3.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day3 = Pmw.EntryField(self.frt_group3.interior(),  # day3
                                  labelpos = 'n', label_text = 'Days after plt',
                                  validate = {'validator': 'numeric', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # # -fertilizer material
    # self._Setting.DSSATSetup2.FA_fert_mat3 = Pmw.ComboBox(self.frt_group3.interior(),  # fert_mat2
    #                                  label_text = 'Material', labelpos = 'n',
    #                                  listbox_width = 15, dropdown = 1,
    #                                  scrolledlist_items = material_list,
    #                                  entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_mat3.selectitem(material_list[7])
    # # -fertilizer application method
    # self._Setting.DSSATSetup2.FA_fert_app_method3 = Pmw.ComboBox(self.frt_group3.interior(),  # fert_app2
    #                                         label_text = 'Method', labelpos = 'n',
    #                                         listbox_width = 15, dropdown = 1,
    #                                         scrolledlist_items = apply_list,
    #                                         entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_app_method3.selectitem(apply_list[5])
    self._Setting.DSSATSetup2.FA_amt_depth3 = Pmw.EntryField(self.frt_group3.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'Depth(cm)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 50, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_N3 = Pmw.EntryField(self.frt_group3.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'N (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_P3 = Pmw.EntryField(self.frt_group3.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'P (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_K3 = Pmw.EntryField(self.frt_group3.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'K (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day3, self._Setting.DSSATSetup2.FA_amt_depth3,
               self._Setting.DSSATSetup2.FA_amt_N3, self._Setting.DSSATSetup2.FA_amt_P3,self._Setting.DSSATSetup2.FA_amt_K3)
    for entry in entries:
      entry.pack(fill = 'x', side = LEFT, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)
    # self._Setting.DSSATSetup2.FA_amt_depth3.setentry("2")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  2cm depth

    # ============4th fertilizer application
    self.frt_group4 = Pmw.Group(self.fert_dialog.interior(), tag_text = '4th application')
    self.frt_group4.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day4 = Pmw.EntryField(self.frt_group4.interior(),  # day3
                                  labelpos = 'n', label_text = 'Days after plt',
                                  validate = {'validator': 'numeric', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # # -fertilizer material
    # self._Setting.DSSATSetup2.FA_fert_mat4 = Pmw.ComboBox(self.frt_group4.interior(),  # fert_mat2
    #                                  label_text = 'Material', labelpos = 'n',
    #                                  listbox_width = 15, dropdown = 1,
    #                                  scrolledlist_items = material_list,
    #                                  entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_mat4.selectitem(material_list[7])
    # # -fertilizer application method
    # self._Setting.DSSATSetup2.FA_fert_app_method4 = Pmw.ComboBox(self.frt_group4.interior(),  # fert_app2
    #                                         label_text = 'Method', labelpos = 'n',
    #                                         listbox_width = 15, dropdown = 1,
    #                                         scrolledlist_items = apply_list,
    #                                         entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_app_method4.selectitem(apply_list[5])
    self._Setting.DSSATSetup2.FA_amt_depth4 = Pmw.EntryField(self.frt_group4.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'Depth(cm)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 50, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_N4 = Pmw.EntryField(self.frt_group4.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'N (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_P4 = Pmw.EntryField(self.frt_group4.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'P (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_K4 = Pmw.EntryField(self.frt_group4.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'K (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day4, self._Setting.DSSATSetup2.FA_amt_depth4,
               self._Setting.DSSATSetup2.FA_amt_N4, self._Setting.DSSATSetup2.FA_amt_P4,self._Setting.DSSATSetup2.FA_amt_K4)
    for entry in entries:
      entry.pack(fill = 'x', side = LEFT, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)
    # self._Setting.DSSATSetup2.FA_amt_depth4.setentry("2")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  2cm depth

    #========5th fertilizer application
    self.frt_group5 = Pmw.Group(self.fert_dialog.interior(), tag_text = '5th application')
    self.frt_group5.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    self._Setting.DSSATSetup2.FA_day5 = Pmw.EntryField(self.frt_group5.interior(),  # day3
                                  labelpos = 'n', label_text = 'Days after plt',
                                  validate = {'validator': 'numeric', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # # -fertilizer material
    # self._Setting.DSSATSetup2.FA_fert_mat5 = Pmw.ComboBox(self.frt_group5.interior(),  # fert_mat2
    #                                  label_text = 'Material', labelpos = 'n',
    #                                  listbox_width = 15, dropdown = 1,
    #                                  scrolledlist_items = material_list,
    #                                  entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_mat5.selectitem(material_list[7])
    # # -fertilizer application method
    # self._Setting.DSSATSetup2.FA_fert_app_method5 = Pmw.ComboBox(self.frt_group5.interior(),  # fert_app2
    #                                         label_text = 'Method', labelpos = 'n',
    #                                         listbox_width = 15, dropdown = 1,
    #                                         scrolledlist_items = apply_list,
    #                                         entryfield_entry_state = DISABLED)
    # self._Setting.DSSATSetup2.FA_fert_app_method5.selectitem(apply_list[5])
    self._Setting.DSSATSetup2.FA_amt_depth5 = Pmw.EntryField(self.frt_group5.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'Depth(cm)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 50, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_N5 = Pmw.EntryField(self.frt_group5.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'N (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_P5 = Pmw.EntryField(self.frt_group5.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'P (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    self._Setting.DSSATSetup2.FA_amt_K5 = Pmw.EntryField(self.frt_group5.interior(),  # self.amount1
                                     labelpos = 'n', label_text = 'K (kg/ha)',
                                     validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},hull_width=5, entry_width=4)
    # aline labels
    entries = (self._Setting.DSSATSetup2.FA_day5, self._Setting.DSSATSetup2.FA_amt_depth5,
               self._Setting.DSSATSetup2.FA_amt_N5, self._Setting.DSSATSetup2.FA_amt_P5,self._Setting.DSSATSetup2.FA_amt_K5)
    for entry in entries:
      entry.pack(fill = 'x', side = LEFT, expand = 1, padx = 10, pady = 2)
    Pmw.alignlabels(entries)
    # self._Setting.DSSATSetup2.FA_amt_depth5.setentry("2")  #!!!!!!!!!!!!!!!!!!=TEMPORARY  2cm depth

    #===========ask if phosphorus simulation is activated or not
    self.frt_group0 = Pmw.Group(self.fert_dialog.interior(), tag_text = '*Phosphorus Simulation',tag_font = Pmw.logicalfont('Helvetica', 0.5))
    self.frt_group0.pack(fill = 'both', side = TOP, expand = 1, padx = 10, pady = 5)
    on_off_list = ("YES", "NO")
    self._Setting.DSSATSetup2.P_button = Pmw.ComboBox(self.frt_group0.interior(),
                                     label_text = 'P simulation?', labelpos = 'wn',
                                     listbox_width = 5, dropdown = 1,scrolledlist_items = on_off_list,
                                     entryfield_entry_state = DISABLED)
    self._Setting.DSSATSetup2.P_button.selectitem(on_off_list[1])
    P_list = ("Very Low(2 ppm)", "Low (7 ppm)", "Medium (12 ppm)", "High (18 ppm)")
    self._Setting.DSSATSetup2.extP = Pmw.ComboBox(self.frt_group0.interior(),
                                     label_text = 'If YES, Extrable P level:', labelpos = 'wn',
                                     listbox_width = 5, dropdown = 1,scrolledlist_items = P_list,
                                     entryfield_entry_state = DISABLED)

    self._Setting.DSSATSetup2.extP.selectitem(P_list[1])
    # aline labels
    entries = (self._Setting.DSSATSetup2.P_button, self._Setting.DSSATSetup2.extP )
    for entry in entries:
      entry.pack(fill = 'x', side = LEFT, expand = 1, padx = 30, pady = 2)
    Pmw.alignlabels(entries)
    self.fert_dialog.withdraw()

    #====================================================================================
    #============to add more specification for automatic irrigation(auto when required)
    self.AutoIrrDialog = Pmw.Dialog(self._UIParent, title = 'Automatic irrigation when required')
    self._Setting.DSSATSetup2.IA_mng_depth = Pmw.EntryField(self.AutoIrrDialog.interior(),  # irr_depth
                                    labelpos = 'w', label_text = 'Management depth(cm):',
                                    validate = {'validator': 'real', 'min': 0, 'max': 200, 'minstrict': 0},
                                    value = '30')
    # -threshold
    self._Setting.DSSATSetup2.IA_threshold = Pmw.EntryField(self.AutoIrrDialog.interior(),  # self.irr_thresh
                                     labelpos = 'w', label_text = 'Threshold(% of maximum available water triggering irrigation ):',
                                     validate = {'validator': 'real', 'min': 0, 'max': 100, 'minstrict': 0},
                                     value = '50')
    # - Efficiency fraction
    self._Setting.DSSATSetup2.IA_eff_fraction = Pmw.EntryField(self.AutoIrrDialog.interior(),  # eff_fraction
                                                                labelpos = 'w', label_text = 'Efficiency fraction:',
                                                                validate = {'validator': 'real', 'min': 0, 'max': 1, 'minstrict': 0},
                                                                value = '1')
    # aline labels
    entries = (self._Setting.DSSATSetup2.IA_mng_depth, self._Setting.DSSATSetup2.IA_threshold, self._Setting.DSSATSetup2.IA_eff_fraction)
    for entry in entries:
      entry.pack(fill = 'x', expand = 1, padx = 10, pady = 5)
    Pmw.alignlabels(entries)
    self.AutoIrrDialog.withdraw()

 #============to add more specification for Reported irrigation
    self.ReptIrrDialog = Pmw.Dialog(self._UIParent, title='Irrigation on Report Dates')
    self.irr_group1 = Pmw.Group(self.ReptIrrDialog.interior(), tag_text = 'Irrigation Method')
    self.irr_group1.pack(fill = 'both',side=TOP, expand = 1, padx = 10, pady = 5)
    ir_method_list = ("IR004(Sprinkler)", "IR001(Furrow)","IR003(Flood)")
    self._Setting.DSSATSetup2.IR_method = Pmw.ComboBox(self.irr_group1.interior(),
                                     label_text = 'Irrigation method:', labelpos = 'w',
                                     listbox_width = 5, dropdown = 1,scrolledlist_items = ir_method_list,
                                     entryfield_entry_state = DISABLED)
    self._Setting.DSSATSetup2.IR_method.selectitem(ir_method_list[0])
    self._Setting.DSSATSetup2.IR_method.pack(fill = 'x', side=LEFT,padx = 10, pady = 5)
    self.irr_group0 = Pmw.Group(self.ReptIrrDialog.interior(), tag_text = 'Irrigation Applications')
    self.irr_group0.pack(fill = 'both',side=TOP, expand = 1, padx = 10, pady = 5)
    self.fields= []
    self._Setting.DSSATSetup2.ir_m1 = Pmw.EntryField(self.irr_group0.interior(), labelpos = 'n', label_text = 'Month',
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 12, 'minstrict' : 1},hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_m1)
    self._Setting.DSSATSetup2.ir_d1 = Pmw.EntryField(self.irr_group0.interior(), labelpos = 'n', label_text = 'Day',
        validate = {'validator': 'numeric', 'min' : 1, 'max' : 31, 'minstrict' : 1},hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_d1)
    self._Setting.DSSATSetup2.ir_y1 = Pmw.EntryField(self.irr_group0.interior(), labelpos = 'n', label_text = 'Year',
        validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2030, 'minstrict' : 0},hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_y1)
    self._Setting.DSSATSetup2.ir_amt1 = Pmw.EntryField(self.irr_group0.interior(), labelpos = 'n',label_text = 'Amount of water (mm)',
        validate = {'validator': 'numeric', 'min' : 0, 'max' : 300, 'minstrict' : 0},hull_width=5, entry_width=20)
    self.fields.append(self._Setting.DSSATSetup2.ir_amt1)
    for i in range(len(self.fields)):
      self.fields[i].grid(row=0, column=i, padx = 5, pady = 2)
    Pmw.alignlabels(self.fields) #, sticky='e')
    self.fields= []
    self._Setting.DSSATSetup2.ir_m2 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 12, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_m2)
    self._Setting.DSSATSetup2.ir_d2 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 31, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_d2)
    self._Setting.DSSATSetup2.ir_y2 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2030, 'minstrict' : 0},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_y2)
    self._Setting.DSSATSetup2.ir_amt2 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 0, 'max' : 300, 'minstrict' : 0},
                                                    hull_width=5, entry_width=20)
    self.fields.append(self._Setting.DSSATSetup2.ir_amt2)
    for i in range(len(self.fields)):
      self.fields[i].grid(row=1, column=i, padx = 5, pady = 2)
    Pmw.alignlabels(self.fields) #, sticky='e')
    self.fields= []
    self._Setting.DSSATSetup2.ir_m3 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 12, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_m3)
    self._Setting.DSSATSetup2.ir_d3 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 31, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_d3)
    self._Setting.DSSATSetup2.ir_y3 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2030, 'minstrict' : 0},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_y3)
    self._Setting.DSSATSetup2.ir_amt3 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 0, 'max' : 300, 'minstrict' : 0},
                                                    hull_width=5, entry_width=20)
    self.fields.append(self._Setting.DSSATSetup2.ir_amt3)
    for i in range(len(self.fields)):
      self.fields[i].grid(row=2, column=i, padx = 5, pady = 2)
    Pmw.alignlabels(self.fields) #, sticky='e')
    self.fields= []
    self._Setting.DSSATSetup2.ir_m4 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 12, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_m4)
    self._Setting.DSSATSetup2.ir_d4 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 31, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_d4)
    self._Setting.DSSATSetup2.ir_y4 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2030, 'minstrict' : 0},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_y4)
    self._Setting.DSSATSetup2.ir_amt4 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 0, 'max' : 300, 'minstrict' : 0},
                                                    hull_width=5, entry_width=20)
    self.fields.append(self._Setting.DSSATSetup2.ir_amt4)
    for i in range(len(self.fields)):
      self.fields[i].grid(row=3, column=i, padx = 5, pady = 2)
    Pmw.alignlabels(self.fields) #, sticky='e')
    self.fields= []
    self._Setting.DSSATSetup2.ir_m5 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 12, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_m5)
    self._Setting.DSSATSetup2.ir_d5 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1, 'max' : 31, 'minstrict' : 1},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_d5)
    self._Setting.DSSATSetup2.ir_y5 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 1950, 'max' : 2030, 'minstrict' : 0},
                                                    hull_width=5, entry_width=6)
    self.fields.append(self._Setting.DSSATSetup2.ir_y5)
    self._Setting.DSSATSetup2.ir_amt5 = Pmw.EntryField(self.irr_group0.interior(), validate = {'validator': 'numeric', 'min' : 0, 'max' : 300, 'minstrict' : 0},
                                                    hull_width=5, entry_width=20)
    self.fields.append(self._Setting.DSSATSetup2.ir_amt5)
    for i in range(len(self.fields)):
      self.fields[i].grid(row=4, column=i, padx = 5, pady = 2)
    Pmw.alignlabels(self.fields) #, sticky='e')
    self.ReptIrrDialog.withdraw()
                #============to add more specification for automatic irrigation(auto when required)
  def empty_fert_label(self):
    # self._Setting.DSSATSetup2.FA_nfertilizer.clear()
    self.label1_2.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label1_3.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label1_4.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label1_5.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label1_6.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label1_7.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label1_8.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label2_2.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label2_3.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label2_4.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label2_5.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label2_6.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label2_7.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label2_8.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label3_2.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label3_3.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label3_4.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label3_5.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label3_6.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label3_7.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label3_8.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label4_2.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label4_3.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label4_4.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label4_5.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label4_6.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label4_7.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label4_8.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label5_2.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label5_3.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    # self.label5_4.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label5_5.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label5_6.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label5_7.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label5_8.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')

  def empty_irrig_label(self):
    self.label402.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label404.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label408.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label121.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label105.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label106.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label109.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label110.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label113.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label114.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label115.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label116.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label117.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')
    self.label118.configure(text = loc.Msg.Not_added_abbr, bg = 'SystemButtonFace')

  #Get irrigationinformation
  def getIrrInput(self):
      if self._Setting.DSSATSetup2.rbIrrigation.get() == 0:  #Automatic when required
          self.AutoIrrDialog.activate()
          self.label402.configure(text = self._Setting.DSSATSetup2.IA_mng_depth.getvalue(), background = 'honeydew1')
          self.label404.configure(text = self._Setting.DSSATSetup2.IA_threshold.getvalue(), background = 'honeydew1')
          self.label408.configure(text = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue(), background = 'honeydew1')

      elif self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  #on reported dates
          self.ReptIrrDialog.activate()
          self.label121.configure(text = self._Setting.DSSATSetup2.IR_method.getvalue(), background = 'honeydew1')
          #irrigation #1
          if self._Setting.DSSATSetup2.ir_m1.getvalue() != '' and self._Setting.DSSATSetup2.ir_d1.getvalue() != '' and self._Setting.DSSATSetup2.ir_y1.getvalue() != '':
              ir_date = self._Setting.DSSATSetup2.ir_m1.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d1.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y1.getvalue()
              self.label105.configure(text = ir_date, background = 'honeydew1')
              self.label106.configure(text = self._Setting.DSSATSetup2.ir_amt1.getvalue(), background = 'honeydew1')
          #irrgation #2
          if self._Setting.DSSATSetup2.ir_m2.getvalue() != '' and self._Setting.DSSATSetup2.ir_d2.getvalue() != '' and self._Setting.DSSATSetup2.ir_y2.getvalue() != '':
              ir_date = self._Setting.DSSATSetup2.ir_m2.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d2.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y2.getvalue()
              self.label109.configure(text = ir_date, background = 'honeydew1')
              self.label110.configure(text = self._Setting.DSSATSetup2.ir_amt2.getvalue(), background = 'honeydew1')
          #irrigation #3
          if self._Setting.DSSATSetup2.ir_m3.getvalue() != '' and self._Setting.DSSATSetup2.ir_d3.getvalue() != '' and self._Setting.DSSATSetup2.ir_y3.getvalue() != '':
              ir_date = self._Setting.DSSATSetup2.ir_m3.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d3.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y3.getvalue()
              self.label113.configure(text = ir_date, background = 'honeydew1')
              self.label114.configure(text = self._Setting.DSSATSetup2.ir_amt3.getvalue(), background = 'honeydew1')
          #irrigation #4
          if self._Setting.DSSATSetup2.ir_m4.getvalue() != '' and self._Setting.DSSATSetup2.ir_d4.getvalue() != '' and self._Setting.DSSATSetup2.ir_y4.getvalue() != '':
              ir_date = self._Setting.DSSATSetup2.ir_m4.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d4.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y4.getvalue()
              self.label115.configure(text = ir_date, background = 'honeydew1')
              self.label116.configure(text = self._Setting.DSSATSetup2.ir_amt4.getvalue(), background = 'honeydew1')
          #irrigation #5
          if self._Setting.DSSATSetup2.ir_m5.getvalue() != '' and self._Setting.DSSATSetup2.ir_d5.getvalue() != '' and self._Setting.DSSATSetup2.ir_y5.getvalue() != '':
              ir_date = self._Setting.DSSATSetup2.ir_m5.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d5.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y5.getvalue()
              self.label117.configure(text = ir_date, background = 'honeydew1')
              self.label118.configure(text = self._Setting.DSSATSetup2.ir_amt5.getvalue(), background = 'honeydew1')
  #====================================================================
  #Get fertilizer application information
  def getFertInput(self):
    if self._Setting.DSSATSetup2.rbFertApp.get() == 0:
      # self._Setting.DSSATSetup2.FA_nfertilizer.clear()
      self.fert_dialog.activate()
      # if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] != "None":
      if self._Setting.DSSATSetup2.FA_day1.getvalue() != " ":
        self.label1_2.configure(text = self._Setting.DSSATSetup2.FA_day1.getvalue(), background = 'honeydew1')
        # self.label1_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0], background = 'honeydew1')
        # self.label1_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0], background = 'honeydew1')
        self.label1_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth1.getvalue(), background = 'honeydew1')
        self.label1_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N1.getvalue(), background = 'honeydew1')
        self.label1_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P1.getvalue(), background = 'honeydew1')
        self.label1_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K1.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_day2.getvalue() != " ":
        self.label2_2.configure(text = self._Setting.DSSATSetup2.FA_day2.getvalue(), background = 'honeydew1')
        # self.label2_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0], background = 'honeydew1')
        # self.label2_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0], background = 'honeydew1')
        self.label2_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth2.getvalue(), background = 'honeydew1')
        self.label2_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N2.getvalue(), background = 'honeydew1')
        self.label2_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P2.getvalue(), background = 'honeydew1')
        self.label2_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K2.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_day3.getvalue() != " ":
        self.label3_2.configure(text = self._Setting.DSSATSetup2.FA_day3.getvalue(), background = 'honeydew1')
        # self.label3_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0], background = 'honeydew1')
        # self.label3_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0], background = 'honeydew1')
        self.label3_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth3.getvalue(), background = 'honeydew1')
        self.label3_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N3.getvalue(), background = 'honeydew1')
        self.label3_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P3.getvalue(), background = 'honeydew1')
        self.label3_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K3.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_day4.getvalue() != " ":
        self.label4_2.configure(text = self._Setting.DSSATSetup2.FA_day4.getvalue(), background = 'honeydew1')
        # self.label4_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0], background = 'honeydew1')
        # self.label4_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0], background = 'honeydew1')
        self.label4_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth4.getvalue(), background = 'honeydew1')
        self.label4_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N4.getvalue(), background = 'honeydew1')
        self.label4_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P4.getvalue(), background = 'honeydew1')
        self.label4_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K4.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_day5.getvalue() != " ":
        self.label5_2.configure(text = self._Setting.DSSATSetup2.FA_day5.getvalue(), background = 'honeydew1')
        # self.label5_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0], background = 'honeydew1')
        # self.label5_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0], background = 'honeydew1')
        self.label5_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth5.getvalue(), background = 'honeydew1')
        self.label5_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N5.getvalue(), background = 'honeydew1')
        self.label5_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P5.getvalue(), background = 'honeydew1')
        self.label5_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K5.getvalue(), background = 'honeydew1')

  # initialize UI (user interface) with Setting
  def initUI_Setting(self):
    # temp = self._Setting.DSSATSetup2.FA_nfertilizer.getvalue()  #(EJ: 2/24/2020)
    # # Fertilization application
    # self.empty_fert_label()

    if self._Setting.DSSATSetup2.rbFertApp.get() == 0:
      #load no. of fertilizer application from the saved config (EJ: 2/24/2020)
      # self._Setting.DSSATSetup2.FA_nfertilizer.setentry(self._Setting.DSSATSetup2.FA_nfertilizer.getvalue())
      # self._Setting.DSSATSetup2.FA_nfertilizer.setentry(temp)
      if self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0] != "None":
        self.label1_2.configure(text = self._Setting.DSSATSetup2.FA_day1.getvalue(), background = 'honeydew1')
        self.label1_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat1.getvalue()[0], background = 'honeydew1')
        self.label1_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method1.getvalue()[0], background = 'honeydew1')
        self.label1_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth1.getvalue(), background = 'honeydew1')
        self.label1_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N1.getvalue(), background = 'honeydew1')
        self.label1_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P1.getvalue(), background = 'honeydew1')
        self.label1_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K1.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0] != "None":
        self.label2_2.configure(text = self._Setting.DSSATSetup2.FA_day2.getvalue(), background = 'honeydew1')
        self.label2_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat2.getvalue()[0], background = 'honeydew1')
        self.label2_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method2.getvalue()[0], background = 'honeydew1')
        self.label2_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth2.getvalue(), background = 'honeydew1')
        self.label2_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N2.getvalue(), background = 'honeydew1')
        self.label2_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P2.getvalue(), background = 'honeydew1')
        self.label2_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K2.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0] != "None":
        self.label3_2.configure(text = self._Setting.DSSATSetup2.FA_day3.getvalue(), background = 'honeydew1')
        self.label3_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat3.getvalue()[0], background = 'honeydew1')
        self.label3_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method3.getvalue()[0], background = 'honeydew1')
        self.label3_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth3.getvalue(), background = 'honeydew1')
        self.label3_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N3.getvalue(), background = 'honeydew1')
        self.label3_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P3.getvalue(), background = 'honeydew1')
        self.label3_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K3.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0] != "None":
        self.label4_2.configure(text = self._Setting.DSSATSetup2.FA_day4.getvalue(), background = 'honeydew1')
        self.label4_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat4.getvalue()[0], background = 'honeydew1')
        self.label4_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method4.getvalue()[0], background = 'honeydew1')
        self.label4_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth4.getvalue(), background = 'honeydew1')
        self.label4_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N4.getvalue(), background = 'honeydew1')
        self.label4_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P4.getvalue(), background = 'honeydew1')
        self.label4_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K4.getvalue(), background = 'honeydew1')
      if self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0] != "None":
        self.label5_2.configure(text = self._Setting.DSSATSetup2.FA_day5.getvalue(), background = 'honeydew1')
        self.label5_3.configure(text = self._Setting.DSSATSetup2.FA_fert_mat5.getvalue()[0], background = 'honeydew1')
        self.label5_4.configure(text = self._Setting.DSSATSetup2.FA_fert_app_method5.getvalue()[0], background = 'honeydew1')
        self.label5_5.configure(text = self._Setting.DSSATSetup2.FA_amt_depth5.getvalue(), background = 'honeydew1')
        self.label5_6.configure(text = self._Setting.DSSATSetup2.FA_amt_N5.getvalue(), background = 'honeydew1')
        self.label5_7.configure(text = self._Setting.DSSATSetup2.FA_amt_P5.getvalue(), background = 'honeydew1')
        self.label5_8.configure(text = self._Setting.DSSATSetup2.FA_amt_K5.getvalue(), background = 'honeydew1')

    # # Irrigation
    # self.empty_irrig_label()
    #load no. of fertilizer application from the saved config (EJ: 10/23/2020)
    if self._Setting.DSSATSetup2.rbIrrigation.get() == 0: # Automatic when required
      self.label402.configure(text = self._Setting.DSSATSetup2.IA_mng_depth.getvalue(), background = 'honeydew1')
      self.label404.configure(text = self._Setting.DSSATSetup2.IA_threshold.getvalue(), background = 'honeydew1')
      self.label408.configure(text = self._Setting.DSSATSetup2.IA_eff_fraction.getvalue(), background = 'honeydew1')
    elif self._Setting.DSSATSetup2.rbIrrigation.get() == 2:  #on reported dates
      self.label121.configure(text = self._Setting.DSSATSetup2.IR_method.getvalue(), background = 'honeydew1')
      #irrigation #1
      if self._Setting.DSSATSetup2.ir_m1.getvalue() != '' and self._Setting.DSSATSetup2.ir_d1.getvalue() != '' and self._Setting.DSSATSetup2.ir_y1.getvalue() != '':
          ir_date = self._Setting.DSSATSetup2.ir_m1.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d1.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y1.getvalue()
          self.label105.configure(text = ir_date, background = 'honeydew1')
          self.label106.configure(text = self._Setting.DSSATSetup2.ir_amt1.getvalue(), background = 'honeydew1')
      #irrgation #2
      if self._Setting.DSSATSetup2.ir_m2.getvalue() != '' and self._Setting.DSSATSetup2.ir_d2.getvalue() != '' and self._Setting.DSSATSetup2.ir_y2.getvalue() != '':
          ir_date = self._Setting.DSSATSetup2.ir_m2.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d2.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y2.getvalue()
          self.label109.configure(text = ir_date, background = 'honeydew1')
          self.label110.configure(text = self._Setting.DSSATSetup2.ir_amt2.getvalue(), background = 'honeydew1')
      #irrigation #3
      if self._Setting.DSSATSetup2.ir_m3.getvalue() != '' and self._Setting.DSSATSetup2.ir_d3.getvalue() != '' and self._Setting.DSSATSetup2.ir_y3.getvalue() != '':
          ir_date = self._Setting.DSSATSetup2.ir_m3.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d3.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y3.getvalue()
          self.label113.configure(text = ir_date, background = 'honeydew1')
          self.label114.configure(text = self._Setting.DSSATSetup2.ir_amt3.getvalue(), background = 'honeydew1')
      #irrigation #4
      if self._Setting.DSSATSetup2.ir_m4.getvalue() != '' and self._Setting.DSSATSetup2.ir_d4.getvalue() != '' and self._Setting.DSSATSetup2.ir_4.getvalue() != '':
          ir_date = self._Setting.DSSATSetup2.ir_m4.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d4.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y4.getvalue()
          self.label115.configure(text = ir_date, background = 'honeydew1')
          self.label116.configure(text = self._Setting.DSSATSetup2.ir_amt4.getvalue(), background = 'honeydew1')
      #irrigation #5
      if self._Setting.DSSATSetup2.ir_m5.getvalue() != '' and self._Setting.DSSATSetup2.ir_d5.getvalue() != '' and self._Setting.DSSATSetup2.ir_y5.getvalue() != '':
          ir_date = self._Setting.DSSATSetup2.ir_m5.getvalue().rjust(2)+'/'+self._Setting.DSSATSetup2.ir_d5.getvalue().rjust(2) + '/'+self._Setting.DSSATSetup2.ir_y5.getvalue()
          self.label117.configure(text = ir_date, background = 'honeydew1')
          self.label118.configure(text = self._Setting.DSSATSetup2.ir_amt5.getvalue(), background = 'honeydew1')
#====================================================================