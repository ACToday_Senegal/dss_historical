# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##===================================================================
"""

from SIMAGRI._compact import *
from SIMAGRI._configuration import *

from SIMAGRI.util import tkinterUtil

# from SIMAGRI.ui.simulationsetup import SimulationSetupUI   #no need for historical run
# from SIMAGRI.ui.temporaldownscaling import TemporalDownscalingUI
from SIMAGRI.ui.dssatsetup1 import DSSATSetup1UI
from SIMAGRI.ui.dssatsetup2 import DSSATSetup2UI
from SIMAGRI.ui.scenariossetup  import ScenariosSetupUI
# from SIMAGRI.ui.diseasesetup import DISEASESetupUI
from SIMAGRI.ui.about import ABOUTUI

class SIMAGRI_MAIN:

  _SIMAGRI_MAIN_UI_Title = loc.SIMAGRI_MAIN_UI_Title

  _SIMAGRI_MAIN_OptinoDB_File = 'simagri_optionDB.txt'
  _SIMAGRI_MAIN_UI_Logo = 'iri_irsa2.gif'
  _SIMAGRI_MAIN_Configuration_File = 'SIMAGRI.ini'

  _UIRoot = None
  _UIRootNotbook = None

  sf = frame = None  # initially, no frame

  TabUI_SimulationSetup = None
  # TabUI_TemporalDownscaling = None
  TabUI_DSSATSetup1 = None
  TabUI_DSSATSetup2 = None
  TabUI_ScenariosSetup = None
  # TabUI_DISEASESetup = None

  # set a variable for saving variables in all UIs
  _Setting = None

 #initialize the class with __init__ that will be executed at first when a instance of class is created.)
 
  def __init__(self):  # initializing an instance
    self._Setting = Setting()
    pass


  def __initUI__(self):
    # Create and pack a NoteBook.
    self._UIRootNotbook = Pmw.NoteBook(self._UIRoot)
    self._UIRootNotbook.pack(fill = 'both', expand = 1, padx = 10, pady = 10)

    # self.TabUI_SimulationSetup = SimulationSetupUI(self._Setting, self._UIRoot, self._UIRootNotbook)
    # self.TabUI_TemporalDownscaling = TemporalDownscalingUI(self._Setting, self._UIRoot, self._UIRootNotbook)
    self.TabUI_DSSATSetup1 = DSSATSetup1UI(self._Setting, self._UIRoot, self._UIRootNotbook)
    self.TabUI_DSSATSetup2 = DSSATSetup2UI(self._Setting, self._UIRoot, self._UIRootNotbook)
    # self.TabUI_DISEASESetup = DISEASESetupUI(self._Setting, self._UIRoot, self._UIRootNotbook)
    self.TabUI_ScenariosSetup = ScenariosSetupUI(self._Setting, self._UIRoot, self._UIRootNotbook)
    self.TabUI_ABOUT = ABOUTUI(self._Setting, self._UIRoot, self._UIRootNotbook)

    self._UIRootNotbook.setnaturalsize()

  def Run(self):
    self._UIRoot = tkinter.Tk()  # The main toplevel referred to as the "root" (Grayson p.32)
    # Setting application-wide defaults for fonts by opening a file, called optionDB (Grayson p49)
    self._UIRoot.option_readfile(os.path.join(CurCWD, self._SIMAGRI_MAIN_OptinoDB_File))
    self._UIRoot.minsize(width = 1000, height = 750) # @ref: http://stackoverflow.com/questions/21958534/setting-the-window-to-a-fixed-size-with-tkinter

    Pmw.initialise(self._UIRoot)
    self._UIRoot.title(self._SIMAGRI_MAIN_UI_Title)  # add title = 'SIMAGRI User-Interface' on the main window

    # Add a logo image as a button
    img = PhotoImage(file = os.path.join(CurCWD, self._SIMAGRI_MAIN_UI_Logo))
    logo_button = Button(self._UIRoot, background = 'white', image = img).pack(side = TOP)

    self.__initUI__()  # Calling SIMAGRI class as a function

    # Add an Exit button to destroy the main window
    fm31 = Frame(self._UIRoot)
    loadConfButton = tkinter.Button(fm31, text=' Load ', command = self.SIMAGRI_MAIN_LOAD_CONFIGURATION)
    loadConfButton.pack(side = LEFT, padx=10)
    saveConfButton = tkinter.Button(fm31, text=' Save ', command = self.SIMAGRI_MAIN_SAVE_CONFIGURATION)
    saveConfButton.pack(side = LEFT, padx=10)
    fm31.pack(side=TOP, padx=5, pady=5)

    exitButton = tkinter.Button(self._UIRoot, text = 'Exit', command = self.SIMAGRI_MAIN_DESTORY)
    exitButton.pack(side = TOP)

    # Center the main window on the screen
    tkinterUtil.Window_ScreenCenter(self._UIRoot)

    # call tkinter mainloop to process events and keep the display activated
    self._UIRoot.mainloop()
    # or
    #while True:
    #  self._UIRoot.update_idletasks()
    #  self._UIRoot.update()



  def SIMAGRI_MAIN_LOAD_CONFIGURATION(self):
    result = tkinter.messagebox.askquestion(loc.SIMAGRI_MAIN_UI_Title, loc.Main.Load_Configuration, icon = 'question')
    if result == 'yes':
      # load configuration
      load_configuration()

      # set variables' values in memory from configuration
      self._Setting.loadConfig()

      # initialize UI with Setting
      # self.TabUI_SimulationSetup.initUI_Setting()  #no need for historical run
      # self.TabUI_TemporalDownscaling.initUI_Setting()
      self.TabUI_DSSATSetup1.initUI_Setting()
      self.TabUI_DSSATSetup2.initUI_Setting()
      # self.TabUI_DISEASESetup.initUI_Setting()
      self.TabUI_ScenariosSetup.initUI_Setting()

  def SIMAGRI_MAIN_SAVE_CONFIGURATION(self):
    result = tkinter.messagebox.askquestion(loc.SIMAGRI_MAIN_UI_Title, loc.Main.Save_Configuration, icon = 'question')
    if result == 'yes':
      # set variables' values in memory into configuration
      self._Setting.saveConfig()

      # save configuration file
      save_configuration()

  def SIMAGRI_MAIN_DESTORY(self):
    # destroy main window
    self._UIRoot.destroy()
