# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._configuration import *

class UIVarDSSATSetup1():

  _Section_Name = 'DSSATSetup1'    ## _single_leading_underscore => private variable
  # Station with Historical Weather Data
  WStation = None
  avail_year1 = None
  avail_year2 = None
  sim_year1 = None #simulation start year
  sim_year2 = None #simulation end year

  #Crop type
  Crop_type = None # RButton2
# ===1) crop informatoin for maize from dialog
  #Maize input
  MZ_soil_type = None  
  MZ_cul_type = None  
  MZ_ini_H2O = None  
  MZ_ini_NO3 = None 

  #Drybean input
  BN_soil_type = None  
  BN_cul_type = None  
  BN_ini_H2O = None  
  BN_ini_NO3 = None 


  # from main page
  Soil_Type = None #from main page
  CUL_Cal_CulName = None # calib_cul
  IC_H2O = None
  IC_NO3 = None
  plt_date = None
  plt_month = None
  plt_density = None

  def __init__(self):
    pass

  def initVars(self):
    pass

  # load from configuration file
  def loadConfig(self):
    #weather station
    get_config_value_into_pmw_combobox(self._Section_Name, 'WStation', self.WStation) 
    get_config_value_into_tkinter_label(self._Section_Name, 'avail_year1', self.avail_year1) # available start year of the selected station
    get_config_value_into_tkinter_label(self._Section_Name, 'avail_year2', self.avail_year2)   # available last year of the selected station
    get_config_value_into_pmw_entryfield(self._Section_Name, 'sim_year1', self.sim_year1) #sim start year
    get_config_value_into_pmw_entryfield(self._Section_Name, 'sim_year2', self.sim_year2) #sim end year

    # # crop to plant
    get_config_value_into_tkinter_intvar(self._Section_Name, 'Crop_type', self.Crop_type) #

    #from labels
    get_config_value_into_tkinter_label(self._Section_Name, 'Soil_Type', self.Soil_Type)
    get_config_value_into_tkinter_label(self._Section_Name, 'CUL_Cal_CulName', self.CUL_Cal_CulName)
    get_config_value_into_tkinter_label(self._Section_Name, 'IC_H2O', self.IC_H2O)
    get_config_value_into_tkinter_label(self._Section_Name, 'IC_NO3', self.IC_NO3)
    get_config_value_into_tkinter_label(self._Section_Name, 'Plt_date', self.Plt_date)

    # # from MZ dialog
    # get_config_value_into_pmw_combobox(self._Section_Name, 'MZ_soil_type', self.MZ_soil_type) #soil_type
    # get_config_value_into_pmw_combobox(self._Section_Name, 'MZ_cul_type', self.MZ_cul_type) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'MZ_ini_H2O', self.MZ_ini_H2O) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'MZ_ini_NO3', self.MZ_ini_NO3) 
    # get_config_value_into_pmw_entryfield(self._Section_Name,'MZ_plt_date', self.MZ_plt_date)         

    # # from BN dialog
    # get_config_value_into_pmw_combobox(self._Section_Name, 'BN_soil_type', self.BN_soil_type) #soil_type
    # get_config_value_into_pmw_combobox(self._Section_Name, 'BN_cul_type', self.BN_cul_type) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'BN_ini_H2O', self.BN_ini_H2O) 
    # get_config_value_into_pmw_combobox(self._Section_Name, 'BN_ini_NO3', self.BN_ini_NO3) 
    # get_config_value_into_pmw_entryfield(self._Section_Name, 'BN_plt_date', self.BN_plt_date)  

 # save into configration file
  def saveConfig(self):
    #weather station
    set_config_value_from_pmw_combobox(self._Section_Name, 'WStation', self.WStation) 
    set_config_value_from_tkinter_label(self._Section_Name, 'avail_year1', self.avail_year1 ) # available start year of the selected station
    set_config_value_from_tkinter_label(self._Section_Name, 'avail_year2', self.avail_year2)   # available last year of the selected station
    set_config_value_from_pmw_entryfield(self._Section_Name, 'sim_year1', self.sim_year1) #sim start year
    set_config_value_from_pmw_entryfield(self._Section_Name, 'sim_year2', self.sim_year2) #sim end year

    # # crop to plant
    set_config_value_from_tkinter_intvar(self._Section_Name, 'Crop_type', self.Crop_type) 

    #from labels
    set_config_value_from_tkinter_label(self._Section_Name, 'Soils_Type', self.Soil_Type)
    set_config_value_from_tkinter_label(self._Section_Name, 'CUL_Cal_CulName', self.CUL_Cal_CulName)
    set_config_value_from_tkinter_label(self._Section_Name, 'IC_H2O', self.IC_H2O)
    set_config_value_from_tkinter_label(self._Section_Name, 'IC_NO3', self.IC_NO3)
    set_config_value_from_tkinter_label(self._Section_Name, 'Plt_date', self.Plt_date)

    # # from MZ dialog
    # set_config_value_from_pmw_combobox(self._Section_Name, 'MZ_soil_type', self.MZ_soil_type) #soil_type
    # set_config_value_from_pmw_combobox(self._Section_Name, 'MZ_cul_type', self.MZ_cul_type) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'MZ_ini_H2O', self.MZ_ini_H2O) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'MZ_ini_NO3', self.MZ_ini_NO3) 
    # set_config_value_from_pmw_entryfield(self._Section_Name, 'MZ_plt_date', self.MZ_plt_date)         

    # # from BN dialog
    # set_config_value_from_pmw_combobox(self._Section_Name, 'BN_soil_type', self.BN_soil_type) #soil_type
    # set_config_value_from_pmw_combobox(self._Section_Name, 'BN_cul_type', self.BN_cul_type) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'BN_ini_H2O', self.BN_ini_H2O) 
    # set_config_value_from_pmw_combobox(self._Section_Name, 'BN_ini_NO3', self.BN_ini_NO3) 
    # set_config_value_from_pmw_entryfield(self._Section_Name, 'BN_plt_date', self.BN_plt_date)  

