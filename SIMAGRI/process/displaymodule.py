# -*- coding: utf-8 -*-

from SIMAGRI._compact import *
from SIMAGRI.process.errormessages import ModuleErrorMessage
import SIMAGRI.process.simagri_processes as simagri_procs


class DISPLAY_PROC:

  _UIParent = None
  _Setting = None

  # _Param__File_Prefix = "param_"

  _ModErrMsg = None

  def __init__(self, setting, uiparanet):
    self._Setting = setting
    self._UIParent = uiparanet

    self._ModErrMsg = ModuleErrorMessage(uiparanet)

  # def Yield_boxplot(self,fname, scename): #, cr_list):
  #   count = len(scename)
  #   n_year = 100
  #   yield_n = np.empty([n_year,count])*np.nan     # For Boxplot

  #   #Read Summary.out from all scenario output
  #   yield_obs = []
  #   obs_flag_list = []
  #   for x in range(0, count):
  #     df_OUT=pd.read_csv(fname[x],delim_whitespace=True ,skiprows=3)
  #     HWAM = df_OUT.iloc[:,21].values  #read 21th column only
  #     if HWAM.shape[0] == 101:  #if last line is from the observed weather
  #         yield_n[:,x] = HWAM[:-1]
  #         yield_obs.append(HWAM[-1])
  #         obs_flag_list.append(1)
  #     else:
  #         yield_n[:,x] = HWAM
  #         yield_obs.append(np.nan)
  #         obs_flag_list.append(0)
  #   yield_n[yield_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
  #   # yield_n[yield_n < 0]=np.nan
  #   # print('after filling', yield_n[:,0])
  #   # #To remove 'nan' from data array
  #   # #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
  #   # mask = ~np.isnan(yield_n)
  #   # filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]

  #   #X data for plot
  #   myXList=[i+1 for i in range(count)]

  #   #Plotting
  #   fig = plt.figure()
  #   fig.suptitle('Yield Estimation', fontsize=12, fontweight='bold')

  #   ax = fig.add_subplot(111)
  #   ax.set_xlabel('Scenario',fontsize=12)
  #   ax.set_ylabel('Yield [kg/ha]',fontsize=12)

  #   if sum(obs_flag_list) >= 1:  #if any of scenario has yield based on observed weather
  #     # Plot a line between yields with observed weather
  #     ax.plot(myXList, yield_obs, 'go-')

  #   ax.boxplot(yield_n,labels=scename, showmeans=True, meanline=True, notch=True) #, bootstrap=10000)
  #   # Wdir_path = self._Setting.ScenariosSetup.Working_directory
  #   # fname.append(path.join(Wdir_path, "Yield_boxplot.png")
  #   # plt.savefig(fig_name)
  #   plt.show()


  # def Yield_boxplot_hist(self,fname, fname_hist, scename):
  def Yield_boxplot_hist(self,fname_hist, scename):
    count = len(scename)
    # n_year = 100
    # yield_n = np.empty([n_year,count])*np.nan     # For Boxplot

    # #Read Summary.out from all scenario output
    # yield_obs = []
    # obs_flag_list = []
    # for x in range(0, count):
    #   df_OUT=pd.read_csv(fname[x],delim_whitespace=True ,skiprows=3)
    #   HWAM = df_OUT.iloc[:,21].values  #read 21th column only
    #   if HWAM.shape[0] == 101:  #if last line is from the observed weather
    #       yield_n[:,x] = HWAM[:-1]
    #       yield_obs.append(HWAM[-1])
    #       obs_flag_list.append(1)
    #   else:
    #       yield_n[:,x] = HWAM
    #       yield_obs.append(np.nan)
    #       obs_flag_list.append(0)
    # yield_n[yield_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)

    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    #Read a distinctive year to compare
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_yield = []
    
    yield_hist = np.empty([70,count])*np.nan  #==> 60 is an arbitrary number
    #Read Summary.out from all scenario output
    # max_nyear = 1
    for x in range(0, count):
      df_OUT=pd.read_csv(fname_hist[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  #read 21th column only
      nyear = HWAM.shape[0]
      yield_hist[:nyear,x] = HWAM
      # if nyear > max_nyear: max_nyear = nyear

      #Read a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        PDAT = df_OUT.iloc[:,14].values  #read 14th column only
        doy = repr(PDAT[0])[4:]
        target = repr(target_yr) + doy
        yr_index = np.argwhere(PDAT == int(target))
        TG_yield.append(HWAM[yr_index[0][0]])

    yield_hist[yield_hist < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
    #trim yield_hist so as to make boxplot with variable length of data => use Just use a list of arrays or lists
    filtered_yield= []
    for x in range(0, count):
      temp = yield_hist[:,x]
      filtered_yield.append(temp[~np.isnan(temp)])

    # #trim yield_hist
    # yield_hist = yield_hist[0:max_nyear,:]
    # yield_hist[yield_hist < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)

    #X data for plot
    myXList=[i+1 for i in range(count)]

    #Plotting
    fig = plt.figure()
    fig.suptitle('Yield Estimation', fontsize=12, fontweight='bold')

    ax = fig.add_subplot(111)
    ax.set_xlabel('Scenario',fontsize=12)
    ax.set_ylabel('Yield [kg/ha]',fontsize=12)

    if yr_flag == 1:  #=> EJ(11/30/2020)
      ax.plot(myXList, TG_yield, 'ro', label='selected year') #'ro-')
      ax.legend(loc='upper left') 
    
    # ax.boxplot(yield_hist,labels=scename, showmeans=True, meanline=True, notch=True) #, bootstrap=10000)
    ax.boxplot(filtered_yield,labels=scename, showmeans=True, meanline=True, notch=True) #, bootstrap=10000)
    # Wdir_path = self._Setting.ScenariosSetup.Working_directory
    # fname.append(path.join(Wdir_path, "Yield_boxplot.png")
    # plt.savefig(fig_name)
    plt.show()

    # #plot============================================
    # #regroup matrix for different planting date
    # i_pos = 1
    # fig = plt.figure()
    # fig.suptitle('Yield estimation using historical weather', fontsize=12, fontweight='bold')
    # ax = plt.axes()

    # for i in range(count):
    #   a = yield_hist[:,i].tolist() #.reshape((1,obs_years,1))
    #   # b = yield_n[:,i].tolist()  #.reshape((nrealiz,1))

    #   new_list = []
    #   new_list.append(a)
    #   # new_list.append(b) #=> nested list
    #   # first boxplot pair
    #   bp = plt.boxplot(new_list, positions = [i_pos, i_pos+1], widths = 0.6, showmeans=True, meanline=True, notch=True)
    #   setBoxColors(bp)
    #   i_pos = i_pos + 3

    # #X data for plot
    # myXList=[i+0.5 for i in range(1, 3*count, 3)]
    # if sum(obs_flag_list) >= 1:  #if any of scenario has yield based on observed weather
    #   # Plot a line between yields with observed weather
    #   plt.plot(myXList, yield_obs, 'go-')

    # # set axes limits and labels
    # plt.xlim(0,3*count)
    # ax.set_xticks(myXList)
    # ax.set_xticklabels(scename)
    # ax.set_xlabel('Scenarios',fontsize=12)
    # ax.set_ylabel('Yield [kg/ha]',fontsize=12)
    # # draw temporary red and blue lines and use them to create a legend
    # hB, = plt.plot([1,1],'b-')
    # hR, = plt.plot([1,1],'r-')
    # plt.legend((hB, hR),('Historical', 'SCF'), loc='best')
    # hB.set_visible(False)
    # hR.set_visible(False)
    # # fig.set_size_inches(12, 8)
    # # fig_name = Wdir_path + "\\"+sname+"_output\\"+sname+"_Yield_boxplot2.png"
    # # plt.savefig(fig_name,dpi=100)
    # plt.show()



  def Yield_exceedance(self,fname_hist, scename): #, cr_list):
    count = len(scename)
    n_year = 50 #approximately
    sorted_yield_n = np.empty([n_year,count])*np.nan
    Fx_scf = np.empty([n_year,count])*np.nan
    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    #Read a distinctive year to compare => EJ(11/30/2020)
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_yield = []

    yield_hist = np.empty([70,count])*np.nan  #==> 60 is an arbitrary number
    #Read Summary.out from all scenario output
    max_nyear = 1
    for x in range(0, count):
      df_OUT=pd.read_csv(fname_hist[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  #read 21th column only
      sorted_yield_n[0:len(HWAM),x] = np.sort(HWAM)
      fx_scf = [1.0/len(sorted_yield_n[0:len(HWAM),x])] * len(sorted_yield_n[0:len(HWAM),x]) #pdf
      #Fx_scf = np.cumsum(fx_scf)
      Fx_scf[0:len(HWAM),x] = 1.0-np.cumsum(fx_scf)  #for exceedance curve

      nyear = HWAM.shape[0]
      yield_hist[:nyear,x] = HWAM
      if nyear > max_nyear: max_nyear = nyear

      #Read a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        PDAT = df_OUT.iloc[:,14].values  #read 21th column only
        doy = repr(PDAT[0])[4:]
        target = repr(target_yr) + doy
        yr_index = np.argwhere(PDAT == int(target))
        TG_yield.append(HWAM[yr_index[0][0]])

    #trim yield_hist
    yield_hist = yield_hist[0:max_nyear,:]
    yield_hist[yield_hist < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
    sorted_yield_n[sorted_yield_n < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)

    #4) Plotting yield exceedance curve
    fig = plt.figure()
    fig.suptitle('Yield Exceedance Curve', fontsize=12, fontweight='bold')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Yield [kg/ha]',fontsize=12)
    ax.set_ylabel('Probability of Exceedance [-]',fontsize=12)
    plt.ylim(0, 1)
    for x in range(count):
      ax.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
      #vertical line for the yield with observed weather
      if yr_flag == 1:
        x_data=[TG_yield[x],TG_yield[x]] #only two points to draw a line
        y_data=[0,1]
        temp= repr(target_yr)[2:] + '-' + scename[x]
        ax.plot(x_data,y_data,'-.', label=temp)
        plt.ylim(0, 1)
    box = ax.get_position()  # Shrink current axis by 15%
    ax.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    # fig_name = Wdir_path + "\\" + sname + "_output\\" + sname +"_Yield_exceedance.png"
    # plt.savefig(fig_name)
    plt.show()

  # def WSGD_plot(self,fname, scename, obs_flag_list): #, cr_list):
  #   count = len(scename)
  #   n_days = 180 #approximate growing days
  #   nrealiz = 101 #100 realization + 1 observation case
  #   WSGD_3D = np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
  #   for x in range(0, count):
  #     read_flag = 0  #off
  #     n_count = 0  #count of n weahter realizations
  #     d_count = 0  #day counter during a growing season
  #     with open(fname[x]) as fp:
  #       for line in fp:
  #         if line[18:21] == '  0':   #line[18:21] => DAP
  #           read_flag = 1  #on
  #         #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
  #         if read_flag ==1 and line == '\n':  #after reading last growing day
  #           read_flag = 0 #off
  #           d_count = 0
  #           n_count = n_count + 1
  #         if read_flag == 1:
  #           WSGD_3D[d_count, n_count, x] = float(line[127:133])
  #           d_count = d_count + 1

  #   # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN
  #   WSGD_50 = np.empty((n_days,count,))*np.NAN
  #   WSGD_25 = np.empty((n_days,count,))*np.NAN
  #   WSGD_75 = np.empty((n_days,count,))*np.NAN
  #   WSGD_obs = np.empty((n_days,count,))*np.NAN

  #   #plot
  #   for i in range(count):
  #     # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
  #     WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 50, axis = 1)
  #     WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 25, axis = 1)
  #     WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 75, axis = 1)
  #     if obs_flag_list[i] == 1:
  #       WSGD_obs[:, i] = WSGD_3D[:, -1, i]

  #   #==================================
  #   #1) Plot WSGD Water stress
  #   nrow = math.ceil(count/2.0)
  #   ncol = 2
  #   Position = range(1, nrow*ncol + 1)  # Create a Position index
  #   # Create main figure
  #   fig = plt.figure(1)
  #   fig.suptitle('Water Stress Index (WSGD)')
  #   s_count = 0  #scenario count
  #   # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
  #   for k in range(nrow*ncol):
  #     # add every single subplot to the figure with a for loop
  #     ax = fig.add_subplot(nrow, ncol,Position[k])
  #     xdata = range(len(WSGD_50[:,s_count]))
  #     ax.plot(xdata, WSGD_50[:,s_count], 'b-', label = 'WGEN')
  #     ax.fill_between(xdata, WSGD_25[:, s_count], WSGD_75[:, s_count], alpha=0.2)

  #     # yerr_low = WSGD_50[:, s_count] - WSGD_25[:, s_count]
  #     # yerr_up = WSGD_75[:, s_count] - WSGD_50[:, s_count]
  #     # ax.errorbar(xdata, WSGD_50[:,s_count], yerr=yerr_low, uplims=True, color='b')
  #     # ax.errorbar(xdata, WSGD_50[:,s_count], yerr=yerr_up, lolims=True, color='g')
  #     ax.set_title(scename[s_count]+' (SCF)')
  #     if obs_flag_list[s_count] == 1:
  #       ax.plot(WSGD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
  #     if s_count == 0 or s_count == 1: #display title, axis lable only for the first two scenarios to avoid overlapping
  #       ax.set(xlabel='Days After Planting [DAP]', ylabel='WSGD [-]')
  #     s_count = s_count + 1
  #     if s_count >= count:
  #       break
  #   # for ax in axs.flat:
  #   #   ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
  #   # # Hide x labels and tick labels for top plots and y ticks for right plots.
  #   # for ax in axs.flat:
  #   #   ax.label_outer()
  #   # fig.set_size_inches(13, 8)
  #   # fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
  #   # plt.savefig(fig_name,dpi=100)
  #   plt.show()

  def WSGD_plot_hist(self,fname_hist, scename): #, obs_flag_list): #, cr_list)
    count = len(scename)
    n_days = 180 #approximate growing days
    #==========================================================
    #read water stress from historical weather observations
    #=======================================================
    #Read a distinctive year to compare => EJ(11/30/2020)
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_WSGD_2D = np.empty((n_days,count,))*np.NAN

    nyears = 50  #arbitrary number
    WSGD_3D_hist = np.empty((n_days,nyears,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname_hist[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname_hist[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              WSGD_3D_hist[d_count, n_count, x] = float(line[238:243]) #WFGD   H20 factor,gr   Water factor for growth (0-1)  for WHEAT
            else:
              WSGD_3D_hist[d_count, n_count, x] = float(line[127:133])  #WSGD   H20 stress,ex   Water stress - expansion/partioning/development (0-1)
            #===Read a distinctive year to compare  => EJ(11/30/2020)
            if yr_flag == 1 and line[1:5] == repr(target_yr):
              TG_WSGD_2D[d_count,x] = WSGD_3D_hist[d_count, n_count, x]
            #===end of Read a distinctive year to compare  => EJ(11/30/2020)
            d_count = d_count + 1

    WSGD_50_hist = np.empty((n_days,count,))*np.NAN
    WSGD_25_hist = np.empty((n_days,count,))*np.NAN
    WSGD_75_hist = np.empty((n_days,count,))*np.NAN

    for i in range(count):
      WSGD_50_hist[:, i] = np.nanpercentile(WSGD_3D_hist[:, :, i], 50, axis = 1)
      WSGD_25_hist[:, i] = np.nanpercentile(WSGD_3D_hist[:, :, i], 25, axis = 1)
      WSGD_75_hist[:, i] = np.nanpercentile(WSGD_3D_hist[:, :, i], 75, axis = 1)

    #==================================
    #1) Plot WSGD Water stress
    nrow = math.ceil(count/2.0)
    ncol = 2
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Water Stress Index (WSGD)')
    s_count = 0  #scenario count
    # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    for k in range(nrow*ncol):
      # add every single subplot to the figure with a for loop
      ax = fig.add_subplot(nrow, ncol,Position[k])
      xdata = range(len(WSGD_50_hist[:,s_count]))
      ax.plot(xdata, WSGD_50_hist[:,s_count], 'b-', label = 'Median')
      ax.fill_between(xdata, WSGD_25_hist[:, s_count], WSGD_75_hist[:, s_count], alpha=0.2)

      #===Add a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        xdata2= range(len(TG_WSGD_2D[:,s_count]))
        ax.plot(xdata2, TG_WSGD_2D[:,s_count], 'r-.', label = 'selected year')
      ax.legend(loc='best')   

      ax.set_title(scename[s_count]) #+' (Climatology)')
      ax.set_ylim([0, 1])

      if s_count == 0 or s_count == 1: #display title, axis lable only for the first two scenarios to avoid overlapping
        ax.set(xlabel='Days After Planting [DAP]', ylabel='WSGD [-]')
      s_count = s_count + 1
      if s_count >= count:
        break
    # for ax in axs.flat:
    #   ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
    # # Hide x labels and tick labels for top plots and y ticks for right plots.
    # for ax in axs.flat:
    #   ax.label_outer()
    # fig.set_size_inches(13, 8)
    # fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
    # plt.savefig(fig_name,dpi=100)
    plt.show()
################################################################################
  # def NSTD_plot(self,fname, scename, obs_flag_list): #, cr_list):
  #   count = len(scename)
  #   n_days = 180 #approximate growing days
  #   nrealiz = 101 #100 realization + 1 observation case
  #   NSTD_3D = np.empty((n_days,nrealiz,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
  #   for x in range(0, count):
  #     read_flag = 0  #off
  #     n_count = 0  #count of n weahter realizations
  #     d_count = 0  #day counter during a growing season
  #     with open(fname[x]) as fp:
  #       for line in fp:
  #         if line[18:21] == '  0':   #line[18:21] => DAP
  #           read_flag = 1  #on
  #         #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
  #         if read_flag ==1 and line == '\n':  #after reading last growing day
  #           read_flag = 0 #off
  #           d_count = 0
  #           n_count = n_count + 1
  #         if read_flag == 1:
  #           NSTD_3D[d_count, n_count, x] = float(line[134:140])
  #           d_count = d_count + 1

  #   # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN
  #   NSTD_50 = np.empty((n_days,count,))*np.NAN
  #   NSTD_25 = np.empty((n_days,count,))*np.NAN
  #   NSTD_75 = np.empty((n_days,count,))*np.NAN
  #   NSTD_obs = np.empty((n_days,count,))*np.NAN

  #   #plot
  #   for i in range(count):
  #     # NSTD_avg[:, i] = np.nanmean(NSTD_3D[:, 0:100, i], axis = 1)
  #     NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 50, axis = 1)
  #     NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 25, axis = 1)
  #     NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 75, axis = 1)
  #     if obs_flag_list[i] == 1:
  #       NSTD_obs[:, i] = NSTD_3D[:, -1, i]

  #   #==================================
  #   #1) Plot NSTD Nitrogen stress
  #   nrow = math.ceil(count/2.0)
  #   ncol = 2
  #   Position = range(1, nrow*ncol + 1)  # Create a Position index
  #   # Create main figure
  #   fig = plt.figure(1)
  #   fig.suptitle('Nitrogen Stress Index (NSTD)')
  #   s_count = 0  #scenario count
  #   # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
  #   for k in range(nrow*ncol):
  #     # add every single subplot to the figure with a for loop
  #     ax = fig.add_subplot(nrow, ncol,Position[k])
  #     xdata = range(len(NSTD_50[:,s_count]))
  #     ax.plot(xdata, NSTD_50[:,s_count], 'b-', label = 'WGEN')
  #     ax.fill_between(xdata, NSTD_25[:, s_count], NSTD_75[:, s_count], alpha=0.2)

  #     # yerr_low = NSTD_50[:, s_count] - NSTD_25[:, s_count]
  #     # yerr_up = NSTD_75[:, s_count] - NSTD_50[:, s_count]
  #     # ax.errorbar(xdata, NSTD_50[:,s_count], yerr=yerr_low, uplims=True, color='b')
  #     # ax.errorbar(xdata, NSTD_50[:,s_count], yerr=yerr_up, lolims=True, color='g')
  #     ax.set_title(scename[s_count]+' (SCF)')
  #     if obs_flag_list[s_count] == 1:
  #       ax.plot(NSTD_obs[:, s_count],'x-.', label='w/ obs. weather', color='r')
  #     if s_count == 0 or s_count == 1: #display title, axis lable only for the first two scenarios to avoid overlapping
  #       ax.set(xlabel='Days After Planting [DAP]', ylabel='NSTD [-]')
  #     s_count = s_count + 1
  #     if s_count >= count:
  #       break

  #   # fig.set_size_inches(13, 8)
  #   # fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
  #   # plt.savefig(fig_name,dpi=100)
  #   plt.show()

  def NSTD_plot_hist(self,fname_hist, scename) :#, obs_flag_list): #, cr_list)
    count = len(scename)
    n_days = 180 #approximate growing days
    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    #Read a distinctive year to compare => EJ(11/30/2020)
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_NSTD_2D = np.empty((n_days,count,))*np.NAN

    nyears = 50  #arbitrary number
    NSTD_3D_hist = np.empty((n_days,nyears,count,))*np.NAN # n growing dats * [100 realizations * n scenarios]
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      #determine which crop (maize/sorghum vs. wheat) is simuated
      for file in os.listdir(fname_hist[x][:-12]):
        if file.endswith(".SNX"):
          crop_type = file[2:4]

      with open(fname_hist[x]) as fp:
        for line in fp:
          if line[18:21] == '  0':   #line[18:21] => DAP
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            if crop_type == "WH":
              NSTD_3D_hist[d_count, n_count, x] = float(line[250:255])  #NFPD   N FACTOR,PHOTSN N factor for photosynthesis (0-1) for wheat
            else:
              NSTD_3D_hist[d_count, n_count, x] = float(line[134:140]) #NSTD   N STRESS FACTOR Nitrogen stress factor (0-1) 
            #===Read a distinctive year to compare  => EJ(11/30/2020)
            if yr_flag == 1 and line[1:5] == repr(target_yr):
              TG_NSTD_2D[d_count,x] = NSTD_3D_hist[d_count, n_count, x]
            #===end of Read a distinctive year to compare  => EJ(11/30/2020)
            d_count = d_count + 1

    NSTD_50_hist = np.empty((n_days,count,))*np.NAN
    NSTD_25_hist = np.empty((n_days,count,))*np.NAN
    NSTD_75_hist = np.empty((n_days,count,))*np.NAN

    for i in range(count):
      NSTD_50_hist[:, i] = np.nanpercentile(NSTD_3D_hist[:, :, i], 50, axis = 1)
      NSTD_25_hist[:, i] = np.nanpercentile(NSTD_3D_hist[:, :, i], 25, axis = 1)
      NSTD_75_hist[:, i] = np.nanpercentile(NSTD_3D_hist[:, :, i], 75, axis = 1)

    #==================================
    #1) Plot NSTD Nitrogen stress
    nrow = math.ceil(count/2.0)
    ncol = 2
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Nitrogen Stress Index (NSTD)')
    s_count = 0  #scenario count
    # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    for k in range(nrow*ncol):
      # add every single subplot to the figure with a for loop
      ax = fig.add_subplot(nrow, ncol,Position[k])
      xdata = range(len(NSTD_50_hist[:,s_count]))
      ax.plot(xdata, NSTD_50_hist[:,s_count], 'b-', label = 'Median')
      ax.fill_between(xdata, NSTD_25_hist[:, s_count], NSTD_75_hist[:, s_count], alpha=0.2)

      #===Add a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        xdata2= range(len(TG_NSTD_2D[:,s_count]))
        ax.plot(xdata2, TG_NSTD_2D[:,s_count], 'r-.', label = 'selected year')
      ax.legend(loc='best')  

      ax.set_title(scename[s_count]) #+' (Climatology)')
      ax.set_ylim([0, 1])

      if s_count == 0 or s_count == 1: #display title, axis lable only for the first two scenarios to avoid overlapping
        ax.set(xlabel='Days After Planting [DAP]', ylabel='NSTD [-]')
      s_count = s_count + 1
      if s_count >= count:
        break
    plt.show()

 ##########################################################################
  def weather_plot_hist(self, fname_hist, scename): #, obs_flag_list):
    count = len(scename)
    nyears = 50  #arbitrary number
    n_days = 220 # #approximate growing days
    #Read a distinctive year to compare => EJ(11/30/2020)
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_TMXD_2D = np.empty((n_days,count,))*np.NAN
      TG_TMND_2D = np.empty((n_days,count,))*np.NAN
      TG_SRAD_2D = np.empty((n_days,count,))*np.NAN

    TMXD_3D = np.empty((n_days,nyears,count,))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    TMND_3D = np.empty((n_days,nyears,count,))*np.NAN
    SRAD_3D = np.empty((n_days,nyears,count,))*np.NAN
    # TMND   MIN TEMP °C     Minimum daily temperature (°C)                           .
    # TMXD   MAX TEMP °C     Maximum daily temperature (°C)
    # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
    # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
    # TAVD   AVG TEMP °C     Average daily temperature (°C)
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      with open(fname_hist[x]) as fp:
        for line in fp:
          if line[12:15] == '  1':   #line[12:15] => DAS
          # if line[6:9] == repr(plt_date[i]).zfill(3):   #line[6:9] => DOY
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            TMXD_3D[d_count, n_count, x] = float(line[60:64])
            TMND_3D[d_count, n_count, x] = float(line[67:71])
            SRAD_3D[d_count, n_count, x] = float(line[39:43])
            #===Read a distinctive year to compare  => EJ(11/30/2020)
            if yr_flag == 1 and line[1:5] == repr(target_yr):
              TG_TMXD_2D[d_count,x] = TMXD_3D[d_count, n_count, x]
              TG_TMND_2D[d_count,x] = TMND_3D[d_count, n_count, x]
              TG_SRAD_2D[d_count,x] = SRAD_3D[d_count, n_count, x]
            #===end of Read a distinctive year to compare  => EJ(11/30/2020)
            d_count = d_count + 1

    TMXD_50 = np.empty((n_days,count,))*np.NAN
    TMXD_25 = np.empty((n_days,count,))*np.NAN
    TMXD_75 = np.empty((n_days,count,))*np.NAN
    TMND_50 = np.empty((n_days,count,))*np.NAN
    TMND_25 = np.empty((n_days,count,))*np.NAN
    TMND_75 = np.empty((n_days,count,))*np.NAN
    SRAD_50 = np.empty((n_days,count,))*np.NAN
    SRAD_25 = np.empty((n_days,count,))*np.NAN
    SRAD_75 = np.empty((n_days,count,))*np.NAN

    #plot
    for i in range(count):
      #count n-th day when all n-years values are available
      temp = np.sum(~np.isnan(TMXD_3D[:,:,i]), axis = 1)
      # index = np.argwhere(temp < nyears)
      obs_years = np.max(temp)
      index = np.argwhere(temp < obs_years)
      TMXD_50[0:index[0][0], i] = np.nanpercentile(TMXD_3D[0:index[0][0], 0:obs_years, i], 50, axis = 1)
      TMXD_25[0:index[0][0], i] = np.nanpercentile(TMXD_3D[0:index[0][0], 0:obs_years, i], 25, axis = 1)
      TMXD_75[0:index[0][0], i] = np.nanpercentile(TMXD_3D[0:index[0][0], 0:obs_years, i], 75, axis = 1)
      TMND_50[0:index[0][0], i] = np.nanpercentile(TMND_3D[0:index[0][0], 0:obs_years, i], 50, axis = 1)
      TMND_25[0:index[0][0], i] = np.nanpercentile(TMND_3D[0:index[0][0], 0:obs_years, i], 25, axis = 1)
      TMND_75[0:index[0][0], i] = np.nanpercentile(TMND_3D[0:index[0][0], 0:obs_years, i], 75, axis = 1)
      SRAD_50[0:index[0][0], i] = np.nanpercentile(SRAD_3D[0:index[0][0], 0:obs_years, i], 50, axis = 1)
      SRAD_25[0:index[0][0], i] = np.nanpercentile(SRAD_3D[0:index[0][0], 0:obs_years, i], 25, axis = 1)
      SRAD_75[0:index[0][0], i] = np.nanpercentile(SRAD_3D[0:index[0][0], 0:obs_years, i], 75, axis = 1)
    #==================================
    #1) Plot all three variables (Tmax, Tmin, SRad) together
    nrow = count
    ncol = 3  #each column for Tmax, Tmin and SRAD repectively
    # fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True) #number of subplots are hard-coded
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Temperature and Solar Radiation (historical observations)')
    s_count = 0  #scenario count
    for k in range(nrow*ncol):
      if k%3 == 0:  #first column for Tmax
        # add every single subplot to the figure with a for loop
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(TMXD_50[:,s_count]))
        ax.plot(xdata, TMXD_50[:,s_count], 'b-', label = 'Median')
        ax.fill_between(xdata, TMXD_25[:, s_count], TMXD_75[:, s_count], alpha=0.2)
        #===Add a distinctive year to compare  => EJ(11/30/2020)
        if yr_flag == 1:
          ax.plot(xdata, TG_TMXD_2D[:,s_count], 'r-.', label = 'selected year')
        ax.legend(loc='best') 

        ax.set_title('Tmax - ' + scename[s_count]) #+' (Climatology)')
        ax.set_ylim([15, 40])
        ax.grid(True)
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='Tempeature [\'C]')
      elif k%3 == 1:  #second column for Tmin
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(TMND_50[:,s_count]))
        ax.plot(xdata, TMND_50[:,s_count], 'b-', label = 'Median')
        ax.fill_between(xdata, TMND_25[:, s_count], TMND_75[:, s_count], alpha=0.2)
        #===Add a distinctive year to compare  => EJ(11/30/2020)
        if yr_flag == 1:
          ax.plot(xdata, TG_TMND_2D[:,s_count], 'r-.', label = 'selected year')
        ax.legend(loc='best') 

        ax.set_title('Tmin - ' + scename[s_count]) #+' (Climatology)')
        ax.set_ylim([5, 25])
        ax.grid(True)
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='Tempeature [\'C]')
      else:    #thrid column for SRAD
        ax = fig.add_subplot(nrow, ncol,Position[k])
        xdata = range(len(SRAD_50[:,s_count]))
        ax.plot(xdata, SRAD_50[:,s_count], 'b-', label = 'Median')
        ax.fill_between(xdata, SRAD_25[:, s_count], SRAD_75[:, s_count], alpha=0.2)
        #===Add a distinctive year to compare  => EJ(11/30/2020)
        if yr_flag == 1:
          ax.plot(xdata, TG_SRAD_2D[:,s_count], 'r-.', label = 'selected year')
        ax.legend(loc='best') 

        ax.set_title('SRAD - ' + scename[s_count]) #+' (Climatology)')
        ax.set_ylim([10, 30])
        ax.grid(True)
        if s_count == 0: #display title, axis lable only for the first scenario to avoid overlapping
          ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]')

        s_count = s_count + 1
    plt.show()


##############################################################
  def cum_rain_plot_hist(self, fname_hist, scename): #, obs_flag_list):
    count = len(scename)
    n_days = 220 # #approximate growing days
    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    #Read a distinctive year to compare => EJ(11/30/2020)
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_PREC_2D = np.empty((n_days,count,))*np.NAN

    nyears = 50  #arbitrary number
    PREC_3D_hist = np.empty((n_days,nyears,count,))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    # sim_years = []
    for x in range(0, count):
      read_flag = 0  #off
      n_count = 0  #count of n weather realizations
      d_count = 0  #day counter during a growing season
      # #find n simuation years from *.SNX file
      # path = fname_hist[x][:-11]
      # snx_files = [f for f in os.listdir(path) if f.endswith('.SNX')]
      # snx_name = snx_files[0] #there is only one snx file in the output dir
      # with open(snx_name) as fp:
      #   for line in fp:
      #     if line[0:6] == ' 1 GE':   #line[12:15] => DAS
      #       sim_years.append(int(line[18:20]))

      with open(fname_hist[x]) as fp:
        for line in fp:
          if line[12:15] == '  1':   #line[12:15] => DAS
            read_flag = 1  #on
          #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
          if read_flag ==1 and line == '\n':  #after reading last growing day
            read_flag = 0 #off
            d_count = 0
            n_count = n_count + 1
          if read_flag == 1:
            PREC_3D_hist[d_count, n_count, x] = float(line[16:22])
            #===Read a distinctive year to compare  => EJ(11/30/2020)
            if yr_flag == 1 and line[1:5] == repr(target_yr):
              TG_PREC_2D[d_count,x] = PREC_3D_hist[d_count, n_count, x]
            #===end of Read a distinctive year to compare  => EJ(11/30/2020)
            d_count = d_count + 1

    PREC_50_hist = np.empty((n_days,count,))*np.NAN
    PREC_25_hist = np.empty((n_days,count,))*np.NAN
    PREC_75_hist = np.empty((n_days,count,))*np.NAN
    max_index = 1
    for i in range(count):
      #count n-th day when all n-years values are available
      temp = np.sum(~np.isnan(PREC_3D_hist[:,:,i]), axis = 1)
      nyear = np.max(temp) #temp[0]
      index = np.argwhere(temp < nyear)
      # index = np.argwhere(temp < sim_years[i]) #EJ(11/30/2020)

      if index[0][0] < 60:  #EJ(6/4/2020) 80 days are arbitrary to avoid cases of crop failur in the beginning
        index[0][0] = 60
      # np.cumsum(a,axis=0)
      PREC_50_hist[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D_hist[0:index[0][0], :, i], axis=0), 50, axis = 1)
      PREC_25_hist[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D_hist[0:index[0][0], :, i], axis=0), 25, axis = 1)
      PREC_75_hist[0:index[0][0], i] = np.nanpercentile(np.cumsum(PREC_3D_hist[0:index[0][0], :, i], axis=0), 75, axis = 1)
      if index[0][0] > max_index:
        max_index = index[0][0]
    #trim the array before visualization
    PREC_50_hist = PREC_50_hist[0:max_index,:]
    PREC_25_hist = PREC_25_hist[0:max_index,:]
    PREC_75_hist = PREC_75_hist[0:max_index,:]
    #==================================
    #1) Plot
    nrow = math.ceil(count/2.0)
    ncol = 2  #first column for SCF and the second column for climatology
    Position = range(1, nrow*ncol + 1)  # Create a Position index
    # Create main figure
    fig = plt.figure(1)
    fig.suptitle('Cumulative Precipitation')
    s_count = 0  #scenario count
    for k in range(nrow*ncol):
      # add every single subplot to the figure with a for loop
      ax = fig.add_subplot(nrow, ncol,Position[k])
      xdata = range(len(PREC_50_hist[:,s_count]))
      ax.plot(xdata, PREC_50_hist[:,s_count], 'b-', label = 'Median')
      ax.fill_between(xdata, PREC_25_hist[:, s_count], PREC_75_hist[:, s_count], alpha=0.2)
      #===Add a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        temp = TG_PREC_2D[:,s_count]
        filtered = temp[~np.isnan(temp)]
        xdata2= range(len(filtered))
        ax.plot(xdata2, np.cumsum(filtered), 'r-.', label = 'selected year')
      ax.legend(loc='upper left') 

      ax.grid(True)
      ax.set_title(scename[s_count]) #+' (Climatology)')
      if s_count == 0 or s_count == 1: #display title, axis lable only for the first two scenarios to avoid overlapping
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Precipitation [mm]')
      s_count = s_count + 1
      if s_count >= count:
        break
    plt.show()

#=================================================
#=================================================
# TIME SERIES GRAPH
  def Yield_tseries_hist(self,fname_hist, scename):
    count = len(scename)
    #==========================================================
    #read yields from historical weather observations
    #=======================================================
    #Read a distinctive year to compare
    yr_flag = 0
    year1 = int(self._Setting.DSSATSetup1.sim_year1.getvalue())
    year2 = int(self._Setting.DSSATSetup1.sim_year2.getvalue())
    target_yr = int(self._Setting.ScenariosSetup.comp_year.getvalue())
    if target_yr >= year1 and target_yr <= year2:
      yr_flag = 1
      TG_yield = []
    
    yield_hist = np.empty([70,count])*np.nan  #==> 60 is an arbitrary number
    pdat_hist = np.empty([70,count])*np.nan 
    #Read Summary.out from all scenario output
    # max_nyear = 1
    for x in range(0, count):
      df_OUT=pd.read_csv(fname_hist[x],delim_whitespace=True ,skiprows=3)
      HWAM = df_OUT.iloc[:,21].values  #read 21th column only
      PDAT = df_OUT.iloc[:,14].values  #read 15th column only
      nyear = HWAM.shape[0]
      yield_hist[:nyear,x] = HWAM
      pdat_hist[:nyear,x] = PDAT
      # if nyear > max_nyear: max_nyear = nyear

      #Read a distinctive year to compare  => EJ(11/30/2020)
      if yr_flag == 1:
        PDAT = df_OUT.iloc[:,14].values  #read 14th column only
        doy = repr(PDAT[0])[4:]
        target = repr(target_yr) + doy
        yr_index = np.argwhere(PDAT == int(target))
        TG_yield.append(HWAM[yr_index[0][0]])

    yield_hist[yield_hist < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
    # #trim yield_hist so as to make boxplot with variable length of data => use Just use a list of arrays or lists
    # filtered_yield= []
    # for x in range(0, count):
    #   temp = yield_hist[:,x]
    #   filtered_yield.append(temp[~np.isnan(temp)])

    #Plotting
    fig = plt.figure()
    fig.suptitle('Yield Estimation', fontsize=12, fontweight='bold')

    ax = fig.add_subplot(111)
    ax.set_xlabel('Year',fontsize=12)
    ax.set_ylabel('Yield [kg/ha]',fontsize=12)

    #trim yield_hist
    yield_hist[yield_hist < 0]=0 #==> if HWAM == -99, consider it as "0" yield (i.e., crop failure)
    for x in range(0, count):
      temp = yield_hist[:,x]
      ydata = temp[~np.isnan(temp)]
      xsize = ydata.shape[0]
      xdata = pdat_hist[:xsize,x]
      xdata = xdata//1000  #remove DOY from YYYYDOY
      ax.plot(xdata, ydata, '*:', label=scename[x])

    if yr_flag == 1:  #=> EJ(11/30/2020)
      ax.plot([target_yr, target_yr], [0,2000], 'k-.', label = 'target yr')

    # tidy up the figure
    ax.grid(True)
    ax.legend(loc='best')

    plt.show()
#=================================================
# # function for setting the colors of the box plots pairs
# def setBoxColors(bp):
#     plt.setp(bp['boxes'][0], color='blue')
#     plt.setp(bp['caps'][0], color='blue')
#     plt.setp(bp['caps'][1], color='blue')
#     plt.setp(bp['whiskers'][0], color='blue')
#     plt.setp(bp['whiskers'][1], color='blue')
#     plt.setp(bp['fliers'][0], color='blue')
#     plt.setp(bp['fliers'][1], color='blue')
#     plt.setp(bp['medians'][0], color='blue')

#     plt.setp(bp['boxes'][1], color='red')
#     plt.setp(bp['caps'][2], color='red')
#     plt.setp(bp['caps'][3], color='red')
#     plt.setp(bp['whiskers'][2], color='red')
#     plt.setp(bp['whiskers'][3], color='red')
#     plt.setp(bp['fliers'][1], color='red')
#     plt.setp(bp['fliers'][1], color='red')
#     plt.setp(bp['medians'][1], color='red')

