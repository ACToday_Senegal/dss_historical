# -*- coding: utf-8 -*-

from SIMAGRI._compact import *

__author__ = "Seongkyu Lee"

def getRScriptRunFilename():
  if WindowsOS == True:
    return 'Rscript.exe'
  else:
    return 'Rscript'

def getRScriptPath():
  if WindowsOS == True:
    all_path = os.environ.get('path').split(';')

    rscript_run_path = None

    for tmppath in all_path:
      if(os.path.isfile(os.path.join(tmppath, getRScriptRunFilename()))):
        rscript_run_path = tmppath

    # in Program Files folder
    if rscript_run_path is None:
      try:
        all_path = os.environ.get('programfiles').split(';')

        for tmppath in all_path:
          r_path = os.path.join(tmppath, 'R')
          if os.path.isdir(r_path) == True:
            for dirpath in os.listdir(r_path):
              if (os.path.isfile(os.path.join(tmppath, 'R', dirpath, 'bin', getRScriptRunFilename()))):
                rscript_run_path = os.path.join(tmppath, 'R', dirpath, 'bin')
                break

            if rscript_run_path is None:
              break
      except Exception as e:
        print (e)

    # in Program Files (x86) folder
    if rscript_run_path is None:
      try:
        all_path = os.environ.get('programfiles(x86)').split(';')

        for tmppath in all_path:
          r_path = os.path.join(tmppath, 'R')
          if os.path.isdir(r_path) == True:
            for dirpath in os.listdir(r_path):
              if (os.path.isfile(os.path.join(tmppath, 'R', dirpath, 'bin', getRScriptRunFilename()))):
                rscript_run_path = os.path.join(tmppath, 'R', dirpath, 'bin')
                break

            if rscript_run_path is None:
              break
      except Exception as e:
        print (e)

    return rscript_run_path

  else: # other OS
    all_path = os.environ.get('path').split(':')

    rscript_run_path = None

    for tmppath in all_path:
      if(os.path.isfile(os.path.join(tmppath, getRScriptRunFilename()))):
        rscript_run_path = tmppath

    return rscript_run_path

